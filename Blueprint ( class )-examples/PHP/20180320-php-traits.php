<?php

// trait A {
//   public function do1(){ echo "Hello ";}
//   public function learn(){ echo " I am learning !"; }
//   public function write(){ echo " I am writing ! "; }
// }

// trait B {
//   public function do2(){ echo "World" ;}
//   public function learn(){ echo " I am not learning !"; }
//   public function write(){ echo " I am not writing ! "; }
// }

/******************************************************
 * Multiple traits
 ******************************************************/
// class dosomething{
//   use A, B;
//   public function do12(){
//     echo " <-- Have done ";
//   }
// }

// $k = new dosomething();
// $k->do1();
// $k->do2();
// $k->do12();

/******************************************************
 * Conflict Resolution
 ******************************************************/
// class Conflict{
//   use A, B{
//     A::learn insteadof B;   // use function learn of A instead of B
//     B::write insteadof A;   // use function write of B instead of A
//     B::learn as doLearning; // can use function learn of B with another name
//   }
// }

// $k = new Conflict();
// $k->learn();
// $k->write();
// $k->doLearning();


/******************************************************
 ******************* Presedence
 ******************************************************/
// class Base{
//   public function introduce(){ echo " Who am I ? \n ";}
// }
// trait Take{
//   public function introduce(){
//     parent::introduce();
//     echo " GoGO! \n ";
//   }
// }
// class MyIntroduction extends Base{ use Take; }

// $k = new MyIntroduction();
// $k->introduce();

//=======================================================
// trait Take{
//   public function introduce(){
//     echo " GoGO! \n ";
//   }
// }
// class MyIntroduction {
//   use Take;
//   public function introduce(){
//     echo " FuFunc! \n ";
//   }
// }

// $k = new MyIntroduction();
// $k->introduce(); //   FuFunc!


/******************************************************
 ******************* Change Method Visibility
 ******************************************************/
trait SayOK{
  public function ok(){ echo "OK\n"; }
}

class My{
  use SayOK { ok as protected; }
}

class You{
  use SayOK { ok as private myPrivateOK; }
}

$check = new ReflectionMethod( "My", "ok" );
var_dump( $check->isProtected() ); // ---> true

// Alias method with changed visibility
// sayHello visibility not changed
$check = new ReflectionMethod( "You", "ok" );
var_dump( $check->isPrivate() ); // ---> false



