<?php

class Class1 {
    public function test($arg) {
      echo $arg;
    }
}
class Class2 extends Class1 {
    public function test($arg, $flag) {
      if ($flag) echo '[';
      parent::test($arg);
      if ($flag) echo ']';
    }
}
$o = new Class2();
$o->test('a', 1);
