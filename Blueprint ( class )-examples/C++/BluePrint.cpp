#include <iostream>
#include <string>
using namespace std;

/************************************************
 * For demonstrating inner and outer classes.
 ************************************************/
class Outer{
  class Inner
  {
    public:
      void sayHi(){
        cout << "I am from inner." << endl;
      };
  };

  public:
    void CallInnerHi(){
      Inner in;
      in.sayHi();
    };
};

/************************************************
 * For demonstrating inheritance.
 * Include override.
 ************************************************/
class Area{
  private:
    int Length;
    int Width;
  public:
    Area( int l, int w ){
      Length = l;
      Width = w;
    }
    int CalArea(){
      int area = Length * Width;
      cout << "The Area is " << area << endl;
      return area;
    }
    virtual void DoSomething(){
      cout << "I am from Area." << endl;
    }
};

class Volume: public Area{
  private:
    int Length;
    int Width;
    int Height;
  public:
    Volume( int l, int w, int h )
    : Area( l, w )
    {
      Height = h;
    }
    void CalVol(){
      int vol = Area::CalArea() * Height;
      cout << "The volume is " << vol << endl;
    }
    void DoSomething() override;
};

void Volume::DoSomething(){
  cout << "I am from Volume." << endl;
}

/************************************************
 * For demonstrating abstract class and methods.
 ************************************************/
struct Animal {
    virtual void ShowLegs() = 0;
    void Introduce(){ ShowLegs(); }
};

class Dog: public Animal{
   std::string Name;
   int Legs;
  public:
    Dog( std::string name, int legs ){
      this->Name = name;
      this->Legs = legs;
    }
    void ShowLegs() {
      cout << "My name is " << this->Name << ". My legs are " << this->Legs << endl;
    };
};

class Cat: public Animal{
  std::string Name;
  int Legs;
  public:
    Cat( std::string name, int legs ){
      this->Name = name;
      this->Legs = legs;
    }
    void ShowLegs() {
      cout << "My name is " << this->Name << ". My legs are " << this->Legs << endl;
    };
};

/************************************************
 * For demonstrating Encapsulation
 ************************************************/
class Encap{
  std::string Name;
  std::string Gender;
  int Age;
  public:
    void SetName( std::string name ){
      this->Name = name;
    }
    std::string GetName(){
      return this->Name;
    }
    void SetGender( std::string gender ){
      this->Gender = gender;
    }
    std::string GetGender(){
      return this->Gender;
    }
    void SetAge( int age ){
      this->Age = age;
    }
    int GetAge(){
      return this->Age;
    }
};

/************************************************
 * For demonstrating Interface.
 * A class is made abstract by declaring at least one of its functions
 * as pure virtual function. A pure virtual function is specified by
 * placing "= 0" in its declaration.
 ************************************************/
class ICalc{
  public:
    virtual void calc() = 0;
    void SetNum1( int num ){
      this->Num1 = num;
    }
    void SetNum2( int num ){
      this->Num2 = num;
    }
  protected:
    int Num1;
    int Num2;
};

class Add: public ICalc{
  public:
    void calc(){
      int add = this->Num1 + this->Num2;
      cout << "The sum is " << add << endl;
    }
};

class Sub: public ICalc{
  public:
    void calc(){
      int sub = this->Num1 - this->Num2;
      cout << "The substraction is " << sub << endl;
    }
};

/************************************************
 * For Execution
 ************************************************/
int main(int argc, char const *argv[])
{
  /************************************************
   * usage of inner and outer classes
   ************************************************/
  cout << "---------Inner Class Demo---------" << endl;
  Outer o;
  o.CallInnerHi();

  /************************************************
   * usage of inheritance including override
   ************************************************/
  cout << "---------Inheritance and Override Demo---------" << endl;
  int length = 10;
  int width  = 20;
  int height = 30;
  Volume v( length, width, height );
  v.CalVol();
  v.DoSomething();

  /************************************************
   * usage of abstract
   * c++ does not have 'abstract' keyword
   ************************************************/
  cout << "---------Abstract Demo---------" << endl;
  std::string dname = "puppy";
  int dlegs = 4;
  Dog d( dname, dlegs );
  d.Introduce();

  std::string cname = "mew";
  int clegs = 4;
  Cat c( cname, clegs );
  c.Introduce();

  /************************************************
   * usage of Encapsulation
   ************************************************/
  cout << "---------Encapsulation Demo---------" << endl;
  std::string name = "Alan";
  std::string gender = "Male";
  int age = 18;
  Encap enc;
  enc.SetName( name );
  enc.SetGender( gender );
  enc.SetAge( age );
  cout << "My name is " << enc.GetName() << endl;
  cout << "My gender is " << enc.GetGender() << endl;
  cout << "My age is " << enc.GetAge() << endl;

  /************************************************
   * usage of Interface
   ************************************************/
  cout << "---------Interface Demo---------" << endl;
  int num1 = 10;
  int num2 = 20;
  Add a;
  a.SetNum1( num1 );
  a.SetNum2( num2 );
  a.calc();

  Sub s;
  s.SetNum1( num1 );
  s.SetNum2( num2 );
  s.calc();


  return 0;
}