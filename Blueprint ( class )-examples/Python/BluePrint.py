#################################################
# For demonstrating inner and outer classes.
#################################################
class Outer:
  class Inner:
    def sayHi( self ):
      print( "---------Inner Class Demo---------" )
      print( "Hi, I am from inner." )
  def CallInnerHi( self ):
    _in = self.Inner()
    _in.sayHi()

#################################################
# For demonstrating inheritance.
# Include override.
#################################################
class Area:
  def __init__( self, length, width ):
    self.Length = length
    self.Width = width
  def CalArea( self ):
    area = self.Length * self.Width
    print( "Area is " + str( area ) )
    return area

class Volume( Area ):
  def __init__( self, length, width, height ):
    ### the way for only one parent
    # super( Volume, self ).__init__( length, width )
    ### the way for multiple parents
    Area.__init__( self, length, width )
    self.Height = height
  def CalVol( self ):
    vol = self.CalArea() * self.Height
    print( "My volume is " + str( vol ) )

#################################################
# For demonstrating abstract class and methods.
#################################################
from abc import *
class Animal:
  __metaclass__ = ABCMeta

  @abstractmethod
  def ShowLegs( self ):
    pass
  @abstractmethod
  def Introduce( self ):
    self.ShowLegs()

class Dog( Animal ):
  def __init__( self, Name, Legs ):
    self._Name = Name
    self._Legs = Legs

  def ShowLegs( self ):
    print( "My name is " + str( self._Name ) )
    print( "My legs are " + str( self._Legs ) )

class Cat( Animal ):
  def __init__( self, Name, Legs ):
    self._Name = Name
    self._Legs = Legs

  def ShowLegs( self ):
    print( "My name is " + str( self._Name ) )
    print( "My legs are " + str( self._Legs ) )


#################################################
# For demonstrating encapsulation.
#################################################
#########
# usage 1
#########
class Encap:
  def __init__( self ):
    self.Name = None
    self.Gender = None
    self.Age = None

  def GetName( self ):
    return self.Name
  def SetName( self, name ):
    self.Name = name
  def GetGender( self ):
    return self.Gender
  def SetGender( self, gender ):
    self.Gender = gender
  def GetAge( self ):
    return self.Age
  def SetAge( self, age ):
    self.Age = age
#########
# usage 2
#########
class Encap2:
  def __init__( self ):
    self.Name = None
    self.Gender = None
    self.Age = None

  def GetGender( self ):
    return self.Gender

  def SetGender( self, gen ):
    self.Gender = gen

  gen = property( GetGender, SetGender, "Property gender" )

  #########
  # usage 3
  #########
  @property
  def GetName( self ):
    return self.Name

  @GetName.setter
  def SetName( self, name ):
    self.Name = name

#################################################
# For demonstrating interface.
# ###############################################
# Python has proper multiple inheritance,
# and also ducktyping, so interface is not necessary.
#################################################






#################################################
# For Execution
#################################################
if __name__ == '__main__':
  #################################################
  # usage of inner and outer classes
  #################################################
  o = Outer()
  o.CallInnerHi()

  #################################################
  # usage of inheritance including override
  #################################################
  print( "---------inheritance including override Demo---------" )
  Length = 10
  Width = 20
  Height = 30
  vol = Volume( Length, Width, Height )
  vol.CalVol()

  #################################################
  # usage of abstract
  #################################################
  print( "---------Abstract Demo---------" )
  d = Dog( "puppy", 4 )
  d.Introduce()

  c = Cat( "mew", 4 )
  c.Introduce()

  #################################################
  # usage of Encapsulation
  #################################################
  print( "---------Encapsulation Demo---------" )
  name = "Alan"
  gender = "Male"
  age = 18
  enc = Encap()
  enc.SetName( name )
  enc.SetGender( gender )
  enc.SetAge( age )
  print( "My name is " + str(enc.GetName()) )
  print( "My gender is " + str( enc.GetGender() ) )
  print( "My age is " + str( enc.GetAge() ) )

  enc2 = Encap2()
  enc2.SetName = name
  print( "Encp2, My name is " + enc2.GetName )

  enc2.gen = gender
  print( "Encp2, My gender is " + enc2.gen )



  #################################################
  # usage of Interface
  #################################################
  print( "---------No Interface Demo---------" )



