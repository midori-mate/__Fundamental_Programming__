using System;
using System.Collections.Generic;

namespace BluePrint{
  /************************************************
   * For demonstrating inner and outer classes.
   ************************************************/
  class Outer{
    private class Inner{
      public void sayHi(){
        Console.WriteLine( "---------Inner Class Demo---------" );
        Console.WriteLine( "Hi, I am from inner." );
      }
    }
    public void CallInnerHi(){
      Inner inner = new Inner();
      inner.sayHi();
    }
  }

  /************************************************
   * For demonstrating inheritance.
   * Include override.
   ************************************************/
   class Area{
      private int Length;
      private int Width;

      public Area( int l, int w ){
        this.Length = l;
        this.Width = w;
      }
      public int CalArea(){
        int area = this.Length * this.Width;
        Console.WriteLine( "Area is " + area.ToString() );
        return area;
      }
      public virtual void DoSomething(){
        Console.WriteLine( "I am from Area." );
      }
   }

   class Volume: Area{
      private int Height;
      public Volume( int l, int w, int h ): base( l, w ){
        this.Height = h;
      }
      public void CalVol(){
        int Vol = this.CalArea() * this.Height;
        Console.WriteLine( "Volume is " + Vol.ToString() );
      }
      public override void DoSomething(){
        Console.WriteLine( "I am from Volume." );
      }
   }

  /************************************************
   * For demonstrating abstract class and methods.
   ************************************************/
  abstract class Animal{
    public abstract void ShowLegs();
  }

  class Dog: Animal{
    private string name;
    private int legs;

    public Dog( string name, int legs ){
      this.name = name;
      this.legs = legs;
    }
    public override void ShowLegs(){
      Console.WriteLine( "My name is {0} and my legs are {1}", this.name, this.legs );
    }
  }

  class Cat: Animal{
    private string name;
    private int legs;

    public Cat( string name, int legs ){
      this.name = name;
      this.legs = legs;
    }
    public override void ShowLegs(){
      Console.WriteLine( "My name is {0} and my legs are {1}", this.name, this.legs );
    }
  }

  /************************************************
   * For demonstrating Encapsulation
   ************************************************/
  class Encap{
    private string name;
    private string gender;
    private int age;

    public void SetName( string name ){
      this.name = name;
    }
    public string GetName(){
      return this.name;
    }
    public void SetGender( string gender ){
      this.gender = gender;
    }
    public string GetGender(){
      return this.gender;
    }
    public void SetAge( int age ){
      this.age = age;
    }
    public int GetAge(){
      return this.age;
    }
  }

  /************************************************
   * For demonstrating Interface
   ************************************************/
   interface ICalc{
     // define methods
     void calc();
   }

   class Add : ICalc{
     public int NUM1 = 10;
     public int NUM2 = 20;

     public void calc(){
       Console.WriteLine( "Add result is " + (NUM1 + NUM2).ToString() );
     }
   }

   class Sub : ICalc{
     public int NUM1 = 10;
     public int NUM2 = 20;

     public void calc(){
       Console.WriteLine( "Sub result is " + (NUM1 - NUM2).ToString() );
     }
   }



  /************************************************
   * For Execution
   ************************************************/
  public class BP{
    public static void Main( string[] args ){

      /************************************************
       * usage of inner and outer classes
       ************************************************/
       Outer o = new Outer();
       o.CallInnerHi();

      /************************************************
       * usage of inheritance including override
       ************************************************/
       Console.WriteLine( "---------inheritance including override Demo---------" );
       int Length = 20;
       int Width = 10;
       int Height = 30;
       Volume v = new Volume( Length, Width, Height );
       v.CalVol();
       v.DoSomething();

      /************************************************
       * usage of abstract
       ************************************************/
       Console.WriteLine( "---------abstract Demo---------" );
       List<Animal> AnimalLists = new List<Animal>();
       AnimalLists.Add( new Dog( "puppy", 4 ) );
       AnimalLists.Add( new Cat( "mew", 4 ) );
       foreach( Animal animal in AnimalLists )
          animal.ShowLegs();

      /************************************************
       * usage of Encapsulation
       ************************************************/
       Console.WriteLine( "---------Encapsulation Demo---------" );
       Encap enc = new Encap();
       enc.SetName( "Alan" );
       enc.SetGender( "Male" );
       enc.SetAge( 18 );
       Console.WriteLine( "My name is {0}, gender is {1}, and age is {2}",
                          enc.GetName(),
                          enc.GetGender(),
                          enc.GetAge() );

      /************************************************
       * usage of Interface
       ************************************************/
       Console.WriteLine( "---------Interface Demo---------" );
       Add add = new Add();
       add.calc();
       Sub sub = new Sub();
       sub.calc();


      Console.ReadKey();
    }
  }
}