<?php

include( "./db.php" );

$result = getPrice();
$cost      = $result["cost"];
$remaining = $result["remaining"];

SavePrice( $cost );


function getSummary(){

  $api = BaseUrl() . "btcusd/summary";
  $result = file_get_contents( $api );
  $result = json_decode( $result, true );
  $allowance = $result["allowance"];
  return $allowance;

}

function getPrice(){

  $api = BaseUrl() . "btcusd/price";
  $result = file_get_contents( $api );
  $result = json_decode( $result, true );
  $allowance = $result["allowance"];
  return $allowance;

}

function getOrderBook(){

  $api = BaseUrl() . "btcusd/orderbook";
  $result = file_get_contents( $api );
  $result = json_decode( $result, true );
  $allowance = $result["allowance"];
  return $allowance;

}

function BaseUrl(){

  return "https://api.cryptowat.ch/markets/gdax/";

}
