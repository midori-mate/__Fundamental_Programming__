<?php
//  https://returntrue.win/?level=1


// /// Level 1
// function foo($x)
// {
//     return $x;
// }
// /// Ans
// var_dump( foo( !0 ) );

/// Level 2
// function foo($x)
// {
//     return $x('gehr') === 'true';
// }
// /// Ans
// var_dump( foo(  ) );

// /// Level 3
// function foo($x)
// {
//     return ($x >= 1) && ($x <= 1) && ($x !== 1);
// }
// /// Ans
// var_dump( foo(1.) );


// /// Level 4
// function foo($x)
// {
//     $a = 0;
//     switch ($a) {
//         case $x:
//             return true;
//         case 0:
//             return false;
//     }
//     return false;
// }
// /// Ans
// var_dump( foo(0) );


/// Level 5
// function foo($x)
// {
//     return strlen($x) === 4;
// }
// /// Ans
// var_dump(foo());


// /// Level 6
// function foo($x)
// {
//     return $x === $x();
// }
// /// Ans
// var_dump( foo() );


// /// Level 7
// function foo(stdClass $x)
// {
//     $x = (array) $x;
//     return $x[0];
// }
// /// Ans
// var_dump();



// /// Level 8
// class Bar {}

// function foo(Bar $x)
// {
//     return get_class($x) != 'Bar';
// }
// /// Ans
// var_dump();


// /// Level 9
// function foo($x)
// {
//     $y = $x + 1;
//     return ++$x != $y;
// }
// /// Ans
// var_dump();


// /// Level 10
// class Bar
// {
//     private $a;

//     public function __construct($a)
//     {
//         $this->a = (bool) $a;
//     }

//     public function a()
//     {
//         return $this->a;
//     }
// }

// function foo(callable $x)
// {
//     $object = new Bar(false);
//     $x($object);
//     return $object->a();
// }
// /// Ans
// var_dump();



// /// Level 11
// function foo(array $x)
// {
//     return $x[0] === null;
// }
// /// Ans
// var_dump();



