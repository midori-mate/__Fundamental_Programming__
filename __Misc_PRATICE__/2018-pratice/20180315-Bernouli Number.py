############################
# Bernouli
# 1^k+⋯+n^k modulo 10^9+7
# http://hyoga.hatenablog.com/entry/2018/03/14/113034
############################
import operator as op
from functools import reduce


def nCr(n, r):
    r = min(r, n-r)
    numer = reduce( op.mul, range(n, n-r, -1) )
    denom = reduce( op.mul, range(1, r+1) )
    return numer//denom

def S( k, n ):
  return reduce( lambda a,b: a+(b**k)%(1000000007), range( 1,n+1 ) )


############################
# Execution
############################

if __name__ == '__main__':

  print( nCr(5, 1) )
  # print( S( 1000, 100 ) < 10**9+7 )
