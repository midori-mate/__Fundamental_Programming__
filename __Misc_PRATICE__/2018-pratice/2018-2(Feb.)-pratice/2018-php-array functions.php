<?php
#################################
# array change key case
#################################
// $array = array( "TeSt" => "AppLe", "test1" => "apple" );
// $case = CASE_UPPER;
// // $case = CASE_LOWER;
// print_r( array_change_key_case( $array, $case ) );

// /*  --->Array
//         (
//             [TEST] => AppLe
//             [TEST1] => apple
//         )
// */

#################################
# array chunk ( divide elements of array into several parts averagely )
#################################
// $array = array( "A", "b" => "B", "C", "d" => "D", "E" );
// $IsSameKey = true;
// // $IsSameKey = false;
// $size = 2;
// print_r( array_chunk( $array, $size, $IsSameKey ) );

/*  --->Array
    (
        [0] => Array
            (
                [0] => A
                [b] => B ( if false: [1] => B )
            )
        [1] => Array
            (
                [1] => C ( if false: [0] => C )
                [d] => D ( if false: [1] => D )
            )
        [2] => Array
            (
                [2] => E ( if false: [0] => E )
            )
    )
*/

#################################
# array column ( switch array keys with assigned column_name )
#################################
// $array = array( array( "id" => 1, "name" => "Apple", "age" => 9 ),
//                 array( "id" => 2, "name" => "Bob", "age" => 19 ), );
// $column_key = "name";
// $index_key = "age";
// print_r( array_column( $array, $column_key, $index_key ) );

/*  --->Array
    (
        [9] => Apple
        [19] => Bob
    )
*/

#################################
# array combine
#################################
// $valueTokey_array   = array( "id" => 1, "name" => "Apple", "age" => 9 );
// $valueStillvalue_array = array( "id" => 3, "name" => "Banana", "age" => 29 );

// print_r( array_combine( $valueTokey_array, $valueStillvalue_array ) );

/*  --->Array
    (
        [1] => 3
        [Apple] => Banana
        [9] => 29
    )
*/

#################################
# array count values ( count the number of different elements )
# The lower and upper cases are counted as different things
#################################
// $array   = array( "id" => 1, "name" => "Apple", "age" => 9, "home" => "apple" );

// print_r( array_count_values( $array ) );

/*  --->Array
    (
        [1] => 1
        [Apple] => 1
        [9] => 1
        [apple] => 1
    )
*/


#################################
# array diff assoc ( do set difference )
# key is also compared.
# ---------------------------------------------
# array_diff(): does the same thing but no key comparison.
#################################
// $set   = array( "id" => 1, "name" => "Apple", 9, "home" => "apple" );
// $set1  = array( "id" => 1, "Apple", "age" => 9, 9, "apple", 555 );

// print_r( array_diff_assoc( $set, $set1 ) );

/*  --->Array
    (
        [name] => Apple
        [0] => 9       ( index is 0 since there is no specific key at original set )
        [home] => apple
    )
*/

#################################
# array diff key ( do key's set difference without considering order )
#################################
// $set   = array( "id" => 1, "name" => "Apple", 9, "home" => "apple" );
// $set1  = array( "id" => 1, "Apple", "age" => 9, 9, "apple", 555 );

// print_r( array_diff_key( $set, $set1 ) );

/*  --->Array
    (
        [name] => Apple
        [home] => apple
    )
*/

#################################
# array diff uassoc ( do set difference with self-defined function )
# ---------------------------------------------------------------
# array_diff_ukey: do key's set difference with self-defined function
#################################
// function key_compare_func( $set, $set1 ){
//   if( $set === $set1 ){
//     return 0;
//   }
//   return ( $set > $set1 ) ? 1: -1;
// }

// $set   = array( "id" => 1, "name" => "Apple", 9, "home" => "apple" );
// $set1  = array( "id" => 1, "Apple", "age" => 9, 9, "apple", 555 );

// print_r( array_diff_uassoc( $set, $set1, "key_compare_func" ) );

/*  --->Array
    (
        [name] => Apple
        [0] => 9
        [home] => apple
    )
*/

#################################
# array fill keys ( turn all values of a array into keys of another array
# with assigned value )
#################################
// $set   = array( "id" => 1, "name" => "Apple", 9, "home" => "apple" );
// $value = "skytree";

// print_r( array_fill_keys( $set, $value ) );

/*  --->Array
    (
        [1] => skytree
        [Apple] => skytree
        [9] => skytree
        [apple] => skytree
    )
*/

#################################
# array fill ( build an array with assigned start index, number, and value )
#################################
// $start_index = 3;
// $number = 4;
// $value = "skytree";

// print_r( array_fill( $start_index, $number, $value ) );

/*  --->Array
    (
        [3] => skytree
        [4] => skytree
        [5] => skytree
        [6] => skytree
    )
*/


#################################
# array filter ( filter out unwanted elements with self-defined function )
#################################
// $array = array( 1, 2, 3, 4, 5, 6, 7, 8 );

// function odd( $number ){
//   return ($number & 1);
// }
// function even( $number ){
//   return (!( $number & 1 ));
// }

// // $flag = ARRAY_FILTER_USE_KEY;
// $flag = ARRAY_FILTER_USE_BOTH;

// print_r( array_filter( $array, "odd", $flag ) );

/*  --->Array
    (
      use key          use both
        [1] => 2          [0] => 1
        [3] => 4          [2] => 3
        [5] => 6          [4] => 5
        [7] => 8          [6] => 7
    )
*/

#################################
# array flip ( exchange the key and value of array )
#################################
// $set = array( "A" => 1, "B" => 3, "C" => 6, "Apple" );

// print_r( array_flip( $set ) );

/*  --->Array
    (
        [1] => A
        [3] => B
        [6] => C
        [Apple] => 0
    )
*/

#################################
# array intersect assoc ( do set intersection with comparing both key and value )
# ---------------------------------------------------------------
# array intersect key: compare keys of two sets and return the intersection of key with
#                      corresponding value based on the first set.
# array intersect uassoc: do the same thing as array_intersect_assoc but with user-defined
#                         function not built-in one
# array intersect ukey: the same way as array_intersect_uassoc but do key comparison
# array intersect: do the same thing as array_intersect_assoc but with comparison either
#                  key or value.
#################################
// $set   = array( "id" => 1, "name" => "Apple", 9, "home" => "apple" );
// $set1  = array( "id" => 1, "Apple", "age" => 9, 9, "apple","home" =>  555 );

// print_r( array_intersect_assoc( $set, $set1 ) );

/*  --->Array
    (
        [id] => 1
    )
*/


#################################
# array key exists ( check only whether the key exists or not )
#################################
// $set = array( "A" => 1, "B" => null, "C" => 6, "Apple" );
// $search_key = "A";

// print_r( array_key_exists( $search_key, $set ) );

/*
    1 ---> key exists; else, nothing returned
*/

#################################
# array keys ( return all keys of a given array )
#            ( return the key of a given search value )
#################################
// $set = array( "A" => 1, "B" => null, "C" => 6, "Apple" );
// $search_value = "1";
// $IsStrict = true; // if true, do "===" check, that is, also check the type of variable
// // $IsStrict = false;

// print_r( array_keys( $set, $search_value, $IsStrict ) );

/*
    is strict               not strict
        Array                    Array
            (                        (
            )                        [0] => A
                                     )
*/


#################################
# array map ( do mapping with user-defined function )
#################################
// function Mapping( $x ){
//     return $x**3;
// }

// $Map2 = function ( $x, $y ){
//     return array( $x => $y**3 );
// };

// $set = array( "A" => 1, "B" => 2, "C" => 3 );

// // $Method1 = array_map( function( $key, $value ) {
// //     return array( $key => $value**3 );
// // }, array_keys( $set ), array_values( $set ) );

// $Method1_2 = array_map( $Map2, array_keys( $set ), array_values( $set ) );

// // $Method2 = array_map( "Mapping", $set );

// print_r( $Method1_2 );

/*
  Method 1: (Method1_2)      Method 2:
    Array                      Array
(                              (
    [0] => Array                 [A] => 1
        (                        [B] => 8
            [A] => 1             [C] => 27
        )                      )

    [1] => Array
        (
            [B] => 8
        )

    [2] => Array
        (
            [C] => 27
        )

)
*/

#################################
# array merge recursive ( merge at least two arrays recursively )
#################################
// $set   = array( "id" => 1, "name" => "Apple", "age" => 39, "home" => "apple" );
// $set1  = array( "id" => 5, "Apple", "age" => 9, 9, "apple", 555 );
// $set2  = array( "id" => 3, "Banana", "age" => 29, 555 );

// print_r( array_merge_recursive( $set, $set1 ) );
// print_r( array_merge_recursive( $set, $set1, $set2 ) );

/*  --->Array ( 2 arrays )
    (
        [id] => Array
            (
                [0] => 1
                [1] => 5
            )

        [name] => Apple
        [age] => Array
            (
                [0] => 39
                [1] => 9
            )

        [home] => apple
        [0] => Apple
        [1] => 9
        [2] => apple
        [3] => 555
)
*/


#################################
# array merge ( merge at least two arrays into an array )
#################################
// $set   = array( "A" => 1, "name" => "Apple" );
// $set1  = array( "id" => 5, "age" => 9 );
// $set2  = array( "i5d" => 3, "Banana" => 555 );

// // print_r( array_merge( $set, $set1 ) );
// print_r( array_merge( $set, $set1, $set2 ) );

/*  --->Array ( 2 arrays )
    (
        [A] => 1
        [name] => Apple
        [id] => 5
        [age] => 9
    // if there is third array //
        [i5d] => 3
        [Banana] => 555
    /////////////////////////////
    )
*/


#################################
# array multisort ( sort multiple arrays at the same time based on the order of the
#                   first array )
#                 ( sort an array with assigned order and flag )
#################################
// $set   = array( 3, 1, 2 );
// $set1  = array( "S", "A", "D" );
// $set2  = array( "a" => 1, "d" => 3, "c" => 4 );

// array_multisort( $set, SORT_ASC, SORT_NUMERIC, $set1 );
// // array_multisort( $set, SORT_ASC, SORT_NUMERIC );
// // array_multisort( $set1, SORT_DESC, SORT_STRING );

// print_r( $set );
// print_r( $set1 );


/*  Array
(
    [0] => 1
    [1] => 2
    [2] => 3
)
Array
(
    [0] => A
    [1] => D
    [2] => S
)
*/

#################################
# array pad ( resize a given array with assigned size and value and initial values )
#################################
// $set   = array( "A" => 1, "name" => "Apple" );
// $set1  = array( "id" => 5, "age" => 9 );
// $index = -3;
// $size  = ( $index > 0 ) ? $index + 1 : $index - 1;// '-' means the direction

// print_r( array_pad( $set, $size, $set1 ) );

/*  --->Array ( index > 0 )         ( index < 0 )
    (
        [A] => 1                     [0] => k
        [name] => Apple              [1] => k
        [0] => k                     [A] => 1
        [1] => k                     [name] => Apple
    )
*/

#################################
# array pop ( pop out the last element of an array when called each time )
#           if echo the result directly, the value popped out will be returned.
# ==================================================
# array_push: add several values into target array into the last position of target array.
#             Stack data structure ( LILO: Last in last out )
# array_shift: pop out the first element of an array when called each time
# array_unshift: add several values into the first position of target array.
#                Queue data structure ( FILO: first in last out )
#################################
// $set   = array( "A" => 1, "name" => "Apple", "age" => 9, "id" => 5 );

// print_r( array_pop( $set ) ); //---> return 5
// print_r( $set );

/*  --->Array
    (
        [A] => 1
        [name] => Apple
        [age] => 9
    )
*/


#################################
# array product ( product the all values in the array )
#################################
// $set   = array( 1, 2, 3, 4, 5, 6 );
// $set1  = array( "id" => 5, "age" => 9 );
// $set2  = array( "id", "age" );

// print_r( array_product( $set ) ); //---> 6!
// print_r( array_product( $set1 ) ); //---> 5*9=45
//---------------------------
// print_r( array_product( $set2 ) ); //---> 0
// var_dump( "asa"*1 ); //--->0
// print_r( array_product( array() ) ); //---> 1
//---------------------------


#################################
# array rand ( get an index of element from target array randomly each time )
#################################
// $set   = array( 1, 2, 3, 4, 5, 6 );
// $set1  = array( "id" => 5, "age" => 9 );
// $set2  = array( "id" => 3, "age" => 2 );
// $set3  = array( $set1, $set2 );

// $index = array_rand( $set3 );
// print_r( $index );        //--->0 ( random )
// print_r( $set3[$index] );

/*  --->Array ( random )
    (
        [id] => 5
        [age] => 9
    )
*/


#################################
# array reduce ( do something with user-defined function recursively )
#################################
// $set   = array( 1, 2, 3, 4, 5, 6 );
// $set1  = array( "id" => 5, "age" => 9 );
// $set2  = array( "id", "age" );

// $initial = 7;
// $productfunc = function ( $carry, $item ){
//     $carry *= $item; //the first carry is initial value
//     return $carry;
// };

// $sumfunc = function ( $carry, $item ){
//     $carry += $item; //the first carry is initial value
//     return $carry;
// };

// print_r( array_reduce( $set, $productfunc, $initial ) ); //---> 7*6!
// print_r( array_reduce( $set1, $productfunc, $initial ) ); //---> 7*5*9
// print_r( array_reduce( $set2, $productfunc, $initial ) ); //---> 7*str = 0
// print_r( array_reduce( array(), $productfunc, $initial ) ); //---> 7*1 = 7
// print_r( array_reduce( array(), $sumfunc, $initial ) ); //---> 7+0 = 7
// $message = "No data";
// print_r( array_reduce( array(), $sumfunc, $message ) );//---> No data
// print_r( array_reduce( $set2, $sumfunc, $message ) );//---> 0
// print_r( array_reduce( array(), $productfunc, $message ) );//---> No data
// print_r( array_reduce( $set2, $productfunc, $message ) );//---> 0


#################################
# array replace recursive ( replace the same index of elements of an array recursively 
#             with replacement array)
#             if the same index exists, replace the value; otherwise, add it to basic
#             array.
# ---------------------------------------------------
# array_replace: is different from above, it will completely replace all element with the
#                same index.
#################################
// $base        = array( "id" => 5, array( "age" => 4, "op" => "amp" ), "home" => "Apple" );
// $replacement = array( "id" => 3, array( "age" => 2 ) );

// print_r( array_replace_recursive( $base , $replacement ) );
// print_r( array_replace( $base , $replacement ) );

/*  --->Array ( recursive )           ( not recursive )
    [id] => 3                          [id] => 3
    [0] => Array                       [0] => Array
        (                                  (
            [age] => 2                         [age] => 2
            [op] => amp                    )
        )                              [home] => Apple
    [home] => Apple
*/




#################################
# array reverse ( revers the order of a given array )
#################################
// $set   = array( 1, 2, 3, 4, 5, 6 );
// $set1  = array( "id" => 5, "age" => 9 );
// $set2  = array( "id" => 3, "age" => 2 );
// $set3  = array( $set1, $set2 );

// print_r( array_reverse( $set3 ) );


// /*  --->Array
//     [0] => Array
//         (
//             [id] => 3
//             [age] => 2
//         )

//     [1] => Array
//         (
//             [id] => 5
//             [age] => 9
//         )
// */


#################################
# array search ( find the value whether exists in array or not
#                if exists, return its key )
#               if there are several the same value, the first found key will
#               be returned
# ---------------------------------------------------
# in_array: check the values whether exist in array or not
#################################
// $set  = array( "id" => 5, "age" => 9, "home" => "Apple", "qx" => 9 );

// $keyword = "9";
// // $IsStrict = true;
// $IsStrict = false;

// // print_r( array_search( $keyword, $set, $IsStrict ) );//---> nothing if true; otherwise, age

// print_r( in_array( $keyword, $set, $IsStrict ) );//---> nothing if true; otherwise, 1


#################################
# array slice ( return the assigned range of array )
#################################
// $set  = array( 5, 9, "Apple", 922 );

// $start = -2;
// $num   = 2;
// $IsPreserveKeys = true;
// // $IsPreserveKeys = false;

// print_r( array_slice( $set, $start, $num, $IsPreserveKeys ) );

/*
Array ( false )      true
(
    [0] => Apple       [2] => Apple
    [1] => 922         [3] => 922
)
 */

#################################
# array udiff assoc ( different from array_diff_assoc since user-defined function
#                      is used here )
# -----------------------------------------------------
# array_udiff_uassoc($array1, $array2, "data_compare_func", "key_compare_func")
#################################
// class compare{

//     private $number;
//     function compare( $num ){
//         $this->number = $num;
//     }

//     static function doCompare( $x, $y ){
//         if( $x->number === $y->number ){
//             return 0;
//         }
//         return ( $x->number > $y->number ) ? 1 : -1;
//     }

// }


// function Compare( $x, $y ){
//     if( $x === $y ){
//         return 0;
//     }
//     return ( $x > $y ) ? 1 : -1;
// }
// $set   = array( "id" => 1, "name" => "Apple", 9, "home" => "apple" );
// $set1  = array( "id" => 1, "Apple", "age" => 9, 9, "apple", 555 );
// // print_r( array_udiff_assoc( $set, $set1, "Compare" ) );

// $set   = array( "id" => new compare(1), "name" => new compare("Apple"), new compare(9), "home" => new compare("apple") );
// $set1  = array( "id" => new compare(1), new compare("Apple"), "age" => new compare(9), new compare(9), new compare("apple"), new compare(555) );
// print_r( array_udiff_assoc( $set, $set1, array( "compare", "doCompare" ) ) );

/*
Array ( no class )
(
    [name] => Apple
    [0] => 9
    [home] => apple
)
 */

/*
Array ( class )
(
    [name] => compare Object
        (
            [number:compare:private] => Apple
        )

    [0] => compare Object
        (
            [number:compare:private] => 9
        )

    [home] => compare Object
        (
            [number:compare:private] => apple
        )
)
 */



#################################
# array udiff ( different from array_diff since it uses the user-defined function )
#################################
// function Compare( $x, $y ){
//     if( $x === $y ){
//         return 0;
//     }
//     return ( $x > $y ) ? 1 : -1;
// }

// $set1  = array( "id" => 5, "age" => 9, "home" => 120 );
// $set2  = array( "id" => 3, "age" => 2, "sky" => 120 );

// print_r( array_udiff( $set1, $set2, "Compare" ) );


/*  Array
        (
            [id] => 5
            [age] => 9
        )
*/


#################################
# array uintersect assoc ( different from array_intersect_assoc, it uses with user-defined function )
#################################
// function Compare( $x, $y ){
//     if( $x === $y ){
//         return 0;
//     }
//     return ( $x > $y ) ? 1 : -1;
// }

// $set1  = array( "id" => 5, "age" => 9, "home" => 120 );
// $set2  = array( "id" => 5, "age" => 2, "sky" => 120 );

// print_r( array_udiff_assoc( $set1, $set2, "Compare" ) );


/*  Array
        (
            [age] => 9
            [home] => 120
        )
*/


#################################
# array uintersect uassoc ( it uses user-defined key- and value- compare functions )
#################################
// function value_compare_func( $x, $y ){
//     if( $x === $y ){
//         return 0;
//     }
//     return ( $x > $y ) ? 1 : -1;
// }
// function key_compare_func( $x, $y ){
//     if( $x === $y ){
//         return 0;
//     }
//     return ( $x < $y ) ? 1 : -1;
// }

// $set1  = array( "id" => 5, "age" => 9, "home" => 120 );
// $set2  = array( "id" => 5, "AGE" => 2, "SKY" => 120 );

// print_r( array_udiff_uassoc( $set1, $set2, "value_compare_func", "key_compare_func" ) );


/*  Array
        (
            [age] => 9
            [home] => 120
        )
*/

#################################
# array uintersect ( it uses user-defined value-compare function )
#################################
// function value_compare_func( $x, $y ){
//     if( $x === $y ){
//         return 0;
//     }
//     return ( $x > $y ) ? 1 : -1;
// }

// $set1  = array( "id" => 5, "age" => 9, "home" => 120 );
// $set2  = array( "id" => 5, "AGE" => 2, "SKY" => 120 );

// print_r( array_uintersect( $set1, $set2, "value_compare_func" ) );


/*  Array
        (
            [id] => 5
            [home] => 120
        )
*/


#################################
# array unique ( if the values duplicate, the first found is only displayed )
#################################
// $set1  = array( "id" => 5, "age" => 9, "areg" => 5, "home" => 120 );

// print_r( array_unique( $set1, SORT_NUMERIC ) );


/*  Array
        (
            [id] => 5
            [age] => 9
            [home] => 120
        )
*/


#################################
# array walk recursive ( apply a user-defined function recursively to each member of array )
#################################
// $set1  = array( "id" => 5, "age" => 9, "areg" => 5, "home" => 120 );

// // function DoWalk( $value, $key ){

// //     echo "$key holds $value\r\n";

// // }

// // array_walk_recursive( $set1, "DoWalk" );
// array_walk_recursive( $set1, function( $value, $key ) {
//     echo "$key holds $value\r\n";
// });

/*

id holds 5
age holds 9
areg holds 5
home holds 120

*/



#################################
# array recursive ( apply a user-defined function to each member of array )
#################################
// $set1  = array( "id" => 5, "age" => 9, "areg" => 5, "home" => 120 );

// function DoWalk( $value, $key ){

//     echo "$key holds $value\r\n";

// }

// function AlterWalk( &$value, $key ){

//     $value .= "###";

// }

// // array_walk( $set1, "DoWalk" );
// //------------------------------------------------
// array_walk( $set1, "AlterWalk", "set" );
// array_walk( $set1, "DoWalk" );


/*

id holds 5(### if altered)
age holds 9(### if altered)
areg holds 5(### if altered)
home holds 120(### if altered)

*/


#################################
# arsort ( sort values with descending order )
# --------------------------------------------------------
# asort: with reverse order of arsort
# krsort: sort keys
# ksort: sort keys with ascending order
# rsort: sort an array in reverse order
#################################
// $set1  = array( "id" => 5, "age" => 9, "home" => 120, "jareg" => 5 );

// arsort( $set1, SORT_NUMERIC );
// // asort( $set1, SORT_NUMERIC );

// foreach ( $set1 as $key => $val ){
//     echo "$key is $val\r\n";
// }


/*
home is 120
age is 9
jareg is 5
id is 5
*/

#################################
# compact ()
#################################
// $city = "San";
// $state = "CA";
// $event = "Sqpqpqp";
// $edate = "2018/0214";

// $location_vars = array( "city", "state", "event", "edate" );
// $result = compact( $location_vars );
// print_r( $result );

/*
Array
(
    [city] => San
    [state] => CA
    [event] => Sqpqpqp
    [edate] => 2018/0214
)
*/


#################################
# current( pos ), next, prev, end
# ----------------------------------------
# reset: set the internal pointer of an array to its first element
#################################
// $city = "San";
// $state = "CA";
// $event = "Sqpqpqp";
// $edate = "2018/0214";

// $location_vars = array( "city", "state", "event", "edate" );
// $result = compact( $location_vars );

// var_dump( current( $result ) );
// var_dump( next( $result ) );
// var_dump( next( $result ) );
// var_dump( prev( $result ) );
// var_dump( end( $result ) );

/*
string(3) "San"
string(2) "CA"
string(7) "Sqpqpqp"
string(2) "CA"
string(9) "2018/0214"
*/

#################################
# extract ( reverse of compact )
#################################
// $event = "happy";

// $location_vars = array( "city" => "Apple",
//                         "state" => "queen",
//                         "event" => "king",
//                         "edate" => "lion" );

// extract( $location_vars, EXTR_PREFIX_SAME, "whatup" );
// echo "$city, $state, $event, $edate, $whatup_event";
/*
Apple, queen, happy, lion, king
*/
// extract( $location_vars, EXTR_SKIP );
// echo "$city, $state, $event, $edate";
/*
Apple, queen, happy, lion
*/

#################################
# key ( get the key of assigned value of array )
#################################
// $location_vars = array( "city" => "Apple",
//                         "state" => "queen",
//                         "event" => "king",
//                         "edate" => "lion" );

// while ( $name = current( $location_vars ) ){
//     if( $name == 'Apple' ){
//         echo key( $location_vars );
//     }
//     next( $location_vars );
// }

/*
city
*/

#################################
# list ( list several numbers to be the same values of correspnding index of array )
#################################
// $location_vars = array( "city" ,
//                         "state",
//                         "event",
//                         "edate" );

// list( $fuck, , $userdo,  ) = $location_vars;
// echo $fuck . "----" . $userdo;

/*
city----event
*/

#################################
# natcasesort ( sort array with case insensitive "natural order" )
# -----------------------------------------
# natsort: case sensitive
#################################
// $location_vars = $apple = array( "City" ,
//                                 "state",
//                                 "Event",
//                                 "edate" );
// sort( $location_vars );
// print_r( $location_vars );

// print_r( "<br>" );

// natcasesort( $apple );
// print_r( $apple );

/*
Array
(
    [0] => City
    [1] => Event
    [2] => edate
    [3] => state
)
<br>Array
(
    [0] => City
    [3] => edate
    [2] => Event
    [1] => state
)
*/



#################################
# range
#################################
// $start = 2;
// $end   = 10;
// $step  = 3;

// foreach ( range( $start, $end, $step ) as $value) {
//     echo $value . "-^-";
// }

/*
2-^-5-^-8-^-
 */


#################################
# each
#################################
// $set = array( "a" => "apple", "b" => "banana", "cookie" );

// print_r( each( $set ) );

/*
Array
(
    [1] => apple
    [value] => apple
    [0] => a
    [key] => a
)
 */

// $set = array( "a" => "apple", "b" => "banana", "cookie" );

// while( list( $key, $val ) = each( $set ) ){
//     echo "$key --> $val" . PHP_EOL;
// }
/*
a --> apple
b --> banana
0 --> cookie
*/

#################################
# shuffle ( the result of array will change everytime with the same elements but not order )
#################################
// $set = array( "a" => "apple", "b" => "banana", "cookie" );
// shuffle( $set );
// print_r( $set );

/*
Array
(
    [0] => banana
    [1] => apple
    [2] => cookie
)
*/
