<?php
#################################
# &
#################################
// function NoModify( $param ){
//   return $param++;
// }

// function Modify( &$param ){
//   $param++;
// }

// $param = 1;
// $param = NoModify( $param );
// var_dump( $param ); //-------> int(1)
// Modify( $param );
// var_dump( $param ); //-------> int(2) { what's the difference between with generator? }

//==================================
// Comparison ( & and generator )
//==================================
// function generate( $sup ){
//   for( $i = 1; $i < $sup; ++$i ){
//     yield $i;
//   }
// }

// $sup = 10;
// $gen = generate($sup); //--->1
// var_dump($gen->current());
// $next = $gen->next(); //--->2
// var_dump($gen->current());
// 



// $arr1 = array( "date"=> "2018-01-24 16:09:04", "name" => "alan", "price"=>234.12 );
// $arr2 = array( "date"=> "2018-01-24 16:09:06", "name" => "aqqn", "price"=>313.55 );
// $arr3 = array( "date"=> "2018-01-24 16:09:08", "name" => "qwee", "price"=>999.222 );
// $arr4 = array( "date"=> "2018-01-24 16:09:08", "name" => "qdde", "price"=>219.222 );

// $array = array( $arr1, $arr2, $arr3, $arr4 );

// $array = array_chunk($array, 2);
// for( $i =0; $i< 2; ++$i ){

// }

// print_r( array_merge_recursive( $arr1 ) );

