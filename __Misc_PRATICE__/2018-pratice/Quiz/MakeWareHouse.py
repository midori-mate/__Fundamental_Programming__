from numpy.random import *

def MakeWarehouse( N ):
  warehouse = [ n == n for n in range( N*N ) ]
  obstacles = randint( 0, N*(N+1)/2 )
  obPosition = []
  for o in range( obstacles ):
    randIndex = randint( 0, N*N )
    if randIndex not in obPosition:
      obPosition.append( randIndex )
      warehouse[randIndex] = False
  ouputW = []
  temp = []
  for el in warehouse:
    if el:
      temp.append( "." )
    else:
      temp.append( "#" )
    if len( temp ) == N:
      ouputW.append(temp)
      temp = []
  return ouputW


if __name__ == '__main__':

  print( "\n".join( map( "".join, MakeWarehouse( 5 ) ) ) )
