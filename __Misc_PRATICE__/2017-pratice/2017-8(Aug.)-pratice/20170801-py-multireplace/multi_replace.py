#Replace A with B
def multi_replace(A, B):
    rx = re.compile( "|", join( map( re.escape, B ) ) )
    def one_xlat( match ):
        return B[match.group( 0 )]
    return rx.sub( one_xlat, A )

A = "Happy birthday come on"
B = {
    "Happy" : "Fuck",
    "come"  : "Go",
    "on"    : "out",
}
print (multi_replace(A, B))