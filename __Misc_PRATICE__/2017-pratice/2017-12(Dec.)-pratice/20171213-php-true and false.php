<?php
# It seems that iff when value is null, isset will return false.
# is_null is reverse to isset.
# ! is the same as empty.
$str = "apple";
// var_dump( isset( $str ) );//--->true
// var_dump( empty( $str ) );//--->false
// var_dump( is_null( $str ) );//--->false
// var_dump( !$str );//--->false

$str = "";
// var_dump( isset( $str ) );//--->true
// var_dump( empty( $str ) );//--->true
// var_dump( is_null( $str ) );//--->false
// var_dump( !$str );//--->true

$str = " ";
// var_dump( isset( $str ) );//--->true
// var_dump( empty( $str ) );//--->false
// var_dump( is_null( $str ) );//--->false
// var_dump( !$str );//--->false

$arr = array();
// var_dump( isset( $arr ) );//--->true
// var_dump( empty( $arr ) );//--->true
// var_dump( is_null( $arr ) );//--->false
// var_dump( !$arr );//--->true

$arr = array( "ty" );
// var_dump( isset( $arr ) );//--->true
// var_dump( empty( $arr ) );//--->false
// var_dump( is_null( $arr ) );//--->false
// var_dump( !$arr );//--->false

$null = null;
// var_dump( isset( $null ) );//--->false
// var_dump( empty( $null ) );//--->true
// var_dump( is_null( $null ) );//--->true
// var_dump( !$null );//--->true

$num = -2;
// var_dump( isset( $num ) );//--->true
// var_dump( empty( $num ) );//--->false
// var_dump( is_null( $num ) );//--->false
// var_dump( !$num );//--->false

$num = 0;
// var_dump( isset( $num ) );//--->true
// var_dump( empty( $num ) );//--->true
// var_dump( is_null( $num ) );//--->false
// var_dump( !$num );//--->true