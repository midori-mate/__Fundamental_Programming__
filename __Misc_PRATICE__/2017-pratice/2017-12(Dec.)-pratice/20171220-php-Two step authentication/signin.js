/**************************************************************
###                   Account System                        ###
###############################################################
###############################################################
#######               From 2017/10/05                   #######
###############################################################
###  ##  ##      ##   #######   ##     ####       ####      ###
###  ##  ####  ####    #####    ##  #####  #############  #####
###      ####  ####  #  ###  #  ##    ##  ###       ####  #####
###  ##  ####  ####  ##  #  ##  ##  #####   ##  ##  ####  #####
###  ##  ##      ##  ###   ###  ##     ###      ##  ##      ###
###############################################################
**************************************************************/

/**
 * Check user id and password when he/she signs in.
 *
 */

$(function() {
    $( "button" ).on( "click", function() {

        var user = $( "#user_id" ).val(), pass = $( "#user_password" ).val()
            button = $(this).attr( "id" ), error_mes = [];

        if( button !== "signin" ){
            return false;
        } else {
            if( !user ){ error_mes.push( " Please show me YOUR user ID :) " ); }
            if( !pass ){ error_mes.push( " Please tell me YOUR user PASSWORD 0.0 " ); }

            if( error_mes.length == 0 ){
                $( "#signin_form" ).submit();
            } else {
                alert( error_mes.join( '\n' ) );
                return false;
            }
        }

    });
});