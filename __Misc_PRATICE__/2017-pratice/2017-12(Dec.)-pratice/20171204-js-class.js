
// var op = 27;
// class Test{


//   constructor( x1, x2 ){
//     this.x1 = x1;
//     this.x2 = x2;
//     this.Op = 123;
//   }

//   get Area2(){
//     return this.calcArea();
//   }

//   area( y ){
//     return this.calcArea(y);
//   }

//   calcArea(y){
//     if( !y ){
//       return this.x1*this.x2;
//     }else{
//       return this.x1*this.x2 + y;
//     }
//   }

//   static isOK(x){
//     return "ok" + x;
//   }

//   static k(){
//     return op;
//   }

//   static Go(){
//     return this.k();
//   }

// }

// // var i = new Test( 2, 3 );
// console.log( Test.Go() );

// class Test2{

//   constructor( x1, x2 ){

//     this.x1 = x1;
//     this.x2 = x2;

//   }

//   get Area2(){
//     return this.calcArea();
//   }

//   area( y ){
//     return this.calcArea(y);
//   }

//   calcArea(y){
//     if( !y ){
//       return this.x1*this.x2;
//     }else{
//       return this.x1*this.x2 + y;
//     }
//   }

//   static isOK(x){
//     return Test.isOK(x);
//   }

// }

// var test = new Test( 2, 3 );
// // console.log( test.Area2 );//--->getter cannot have any formal parameter
// // console.log( test.area(7) );//--->put get in front of function and can access this function dynamically.
// console.log( Test2.isOK(4) );//-->static method can be accessed directly by using class name
//-------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------

// var l = [ "MarketCap", "Volume", "Price", "Change" ];
// var k;

// l.forEach( function( s ) {
//     var x = s;
//     k[x] = [];
// });

// console.log( k );
// //-------------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------------

// var k = 123456789;
// var form = new Intl.NumberFormat();
// k = form.format( k );
// console.log( k );
// k = k.replace( /,/g, "" );
// console.log( Number(k) );
// var nf = new Intl.NumberFormat(["en-US"], {
//     style: "currency",
//     currency: "JPY",
//     currencyDisplay: "symbol",
//     maximumFractionDigit: 5
// })

// if (console && console.log) {
//     console.log(nf.format(32125)); // "¥100.00"
// }
// 

