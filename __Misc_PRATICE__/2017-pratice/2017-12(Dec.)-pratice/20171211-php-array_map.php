<?php

$r = array( array( 123, 5.23 ), array( 13, 2.23 ) );
global $t;
$t = 1000;

$result = ToJPYMap( $r );
var_dump( $result );

function ToJPYMap( $data ){

  return array_map( function( $processed_data ) {
    global $t;
    return array( "price" => $processed_data[0] * $t,
                  "size"  => $processed_data[1] );
  }, $data);

}

