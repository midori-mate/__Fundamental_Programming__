class SORT{

  constructor( DataTableTh ){
    this.DataTableTh = DataTableTh;
  }
  /**
   * 表示さてるデータをいったんオブジェクトデータの形で格納して、
   * 再び希望の順でソートした後、表示する。
   */
  SortData(){

    //define variables
        var DataSortKey,
            DataOrderBy,
            DataTable = this.DataTableTh.closest( "table" ),
            DataTableID = DataTable.attr( "id" ),
            DataTableChildren = DataTable.children().children(),
            DataColNums,
            SortColumn,
            DataTempArr = {},  //// get all entries and keep values in DataTempArray;
            ShortNameTemp,
            IDTemp,
            ValueTemp;

        // culumn no you select
        DataSortKey = this.DataTableTh.index();
        // toggle ASC or DESC
        ( this.DataTableTh.hasClass( "DESC" ) ) ? ( DataOrderBy = "ASC" ) : ( DataOrderBy = "DESC" );
        // initialize class
        DataTable.find( "th" ).removeClass( "ASC" );
        DataTable.find( "th" ).removeClass( "DESC" );
        this.DataTableTh.addClass( DataOrderBy );
        DataTable.find( "th" ).children( "span" ).html( "" );
        this.DataTableTh.children( "span" ).html( SORT.ShowArrow( DataOrderBy ) );
        // get number of line and columns of the table
        DataColNums = DataTableChildren.eq(0).children().length;
        //dataを最初のと同じ構造にする
        DataTempArr[DataTableID] = [];
        for( var i = 1; i < (CoinsNum + 1); ++i ){
          ShortNameTemp = DataTableChildren.eq(i).children().eq(1).text();
          ShortNameTemp = ShortNameTemp.replace( " ", "" );
          IDTemp = DataTableChildren.eq(i).children().eq(1).find( "a" ).attr( "href" );
          IDTemp = Catagory.GetIDFromLink( IDTemp );
          ValueTemp = DataTableChildren.eq(i).children().eq(2).text();
          ValueTemp = Catagory.RepMarks( ValueTemp );
          DataTempArr[DataTableID][i-1] = { "coin": ShortNameTemp,
                                            "id"  : IDTemp,
                                            "data": ValueTemp};
        }
        // sort by the key you selected
        switch( Number( DataSortKey ) ){
          // case 0:
          //   SortColumn = "rank";
          //   break;
          case 1:
            SortColumn = "coin";
            break;
          case 2:
          default:
            SortColumn = "data";
            break;
        }
        ( DataOrderBy == "ASC" ) ? ( SORT.RankSort( DataTempArr[DataTableID], SortColumn, -1 ) ) : ( SORT.RankSort( DataTempArr[DataTableID], SortColumn, 1 ) );
        // update sort status
        $( "#" + DataTableID + "_column" ).val( DataSortKey );
        (  DataOrderBy == "ASC" ) ? ( $( "#" + DataTableID + "_order" ).val( -1 ) ) : ( $( "#" + DataTableID + "_order" ).val( 1 ) );
        // insert DataTempArranged values into table
        Catagory.MakeCoinsList( DataTempArr );

  }
  //sortkey = 1降順(DESC), -1昇順(ASC)
  static RankSort( DataTempArr, DataSortKey, sortkey ){

    return DataTempArr.sort( function( a, b ) {
        if( a[DataSortKey] != b[DataSortKey]  ){
          if( a[DataSortKey] < b[DataSortKey] ){
            return ( 1 * sortkey ) ;
          } else {
            return ( -1 * sortkey );
          }
        }
    });

  }

  static GetDataTableSortCond( DataTableID ){
    var DataTableOrder  = $( "#" + DataTableID + "_order" ).val(),
        DataTableColumn = $( "#" + DataTableID + "_column" ).val();
    // 0: rank, 1: coin, 2: data
    switch( Number( DataTableColumn ) ){
      // case 0:
      //   DataTableColumn = "rank";
      //   break;
      case 1:
        DataTableColumn = "coin";
        break;
      case 2:
      default:
        DataTableColumn = "data";
        break;
    }
    return { "order": DataTableOrder, "column": DataTableColumn };
  }

  static ShowArrow( DataOrderBy ){

    switch( DataOrderBy ){
      case "ASC":
        return "&#9650;";
        break;
      case "DESC":
      default:
        return "&#9660;";
        break;
    }

  }

}////class SORT end