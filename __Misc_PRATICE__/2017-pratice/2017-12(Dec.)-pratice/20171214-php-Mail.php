<?php
################### Settings of XAMPP #########################
/********************
 * php.ini setting
 ********************/
#[mail function]
#smtp_port=587
#sendmail_from = 自分のメールアドレス
#sendmail_path = "C:\xampp\sendmail\sendmail.exe -t -i"
/*************************
 * sendmail.ini setting
 *************************/
#smtp_server=mail.sitescope.co.jp
#smtp_port=587
#smtp_ssl=tls
#error_logfile=error_log/error.log
#debug_logfile=debug_log/debug.log
#auth_username=自分のメールアドレス
#auth_password=自分のパスワード
#force_sender=自分のメールアドレス
################################################################
/**
 * Initialize MAILIER
 */
$Sender = "s-himegi@sitescope.co.jp";
$SenderName = "Himegi";
$MAILIER = new MAILIER( $Sender, $SenderName );
/**
 * List main recipients
 */
$Recipients = array();
$Recipients[0]["name"] = "Alan";
$Recipients[0]["mailaddr"] = "abc@hotmail.com";
$Recipients[1]["name"] = "";
$Recipients[1]["mailaddr"] = "paase@hotmail.co.jp";
$MAILIER->MakeMailTo( $Recipients );
/**
 * List Cc recipients
 */
$Recipients = array();
$Recipients[0]["name"] = "Dash";
$Recipients[0]["mailaddr"] = "hsad@hotmail.com";
$Recipients[1]["name"] = "Tiny";
$Recipients[1]["mailaddr"] = "paaseh@otmail.co.jp";
$MAILIER->MakeMailCc( $Recipients );
/**
 * List BCc recipients
 */
$Recipients = array();
$Recipients[0]["name"] = "Daugolous";
$Recipients[0]["mailaddr"] = "god@hot.mail.com";
$Recipients[1]["name"] = "Tim";
$Recipients[1]["mailaddr"] = "paasehot@mail.co.jp";
$MAILIER->MakeMailBCc( $Recipients );
/**
 * Make Subject
 */
$Subject = "Test For Mailing";
$MAILIER->MakeSubject( $Subject );
/**
 * Make body
 */
$Body = "Got it";
$MAILIER->MakeBody( $Body );
/**
 * Make Return
 */
$ReturnMail = "godho@t.mail.com";
$MAILIER->MakeReturn( $ReturnMail );
/**
 * Send Mail
 */
$MAILIER->SendMail();


class MAILIER{

  private $MailFrom;
  private $MailTo;
  private $MailCc;
  private $MailBCc;
  private $MailReturn;
  private $SenderName;
  private $Subject;
  private $Header;
  private $Body;
  private $ErrorMessage = array();
  private $MailStrCode = "ISO-2022-JP";
  private $Encode      = "UTF-8";
  private $Language    = "ja";
  private $ContentType = "\nContent-Type: text/plain;charset=ISO-2022-JP";

  function __construct( $Sender, $SenderName ){

    $this->MailFrom   = $Sender;
    $this->SenderName = $SenderName;

  }

  protected function GetMailFrom(){

    return mb_convert_encoding( " From: " . $this->SenderName, $this->MailStrCode, $this->Encode );

  }

  protected function GetMailTo(){

    return mb_convert_encoding( $this->MailTo,  $this->MailStrCode, $this->Encode );

  }
  /**
   * [MakeMailTo description]
   * @param array $Recipients [ array("name"=>"Alan", "mailaddr"=>"youraddr@example.com") ]
   */
  public function MakeMailTo( array $Recipients ){

    $IsCc = false;
    $IsBCc = false;
    $this->MailTo = implode( ",", $this->MakeMail( $Recipients, $IsCc, $IsBCc ) );

  }

  protected function GetMailCc(){

    return mb_convert_encoding( $this->MailCc,  $this->MailStrCode, $this->Encode );

  }

  public function MakeMailCc( array $Recipients ){

    $IsCc = true;
    $IsBCc = false;
    $this->MailCc = "Cc:" . implode( ",", $this->MakeMail( $Recipients, $IsCc, $IsBCc ) );

  }

  protected function GetMailBCc(){

    return mb_convert_encoding( $this->MailBCc,  $this->MailStrCode, $this->Encode );

  }

  public function MakeMailBCc( array $Recipients ){

    $IsCc = false;
    $IsBCc = true;
    $this->MailBCc = "BCc:" . implode( ",", $this->MakeMail( $Recipients, $IsCc, $IsBCc ) );

  }

  private function MakeMail( array $Recipients, $IsCc, $IsBCc ){

    $MailAddr = array();
    foreach ( $Recipients as $Recipient ) {
      foreach( $Recipient as $key => $value ){
        switch( $key ){
          case "name":
            $$key = $value;
            break;
          case "mailaddr":
            $$key = MAILIER::CheckMailAddr( $value );
            break;
        }
      }
      if( !$name ){ $name = "Mr./Ms."; }
        if( !$mailaddr ){
            if( $IsCc ){
              $ErrMes = "Cc: ";
            } elseif ( $IsBCc ) {
              $ErrMes = "BCc: ";
            } else {
              $ErrMes = "To: ";
            }
            $ErrMes .= $name . "'s mail address is WRONG.";
            array_push( $this->ErrorMessage , $ErrMes );
          } else {
            $MailAddr[] = mb_encode_mimeheader( $name ) . "<" . $mailaddr . ">";
        }
    }
    return $MailAddr;

  }

  public function MakeSubject( $MailSubject ){

    $this->Subject = $MailSubject;

  }

  private function GetSubject(){

    return $this->Subject;

  }

  public function MakeBody( $MailBody ){

    $this->Body = $MailBody;

  }

  private function GetBody(){

    return mb_convert_encoding( $this->Body, $this->MailStrCode, $this->Encode );

  }

  private function GetHeader(){

    $Header  = mb_encode_mimeheader( $this->GetMailFrom() );
    $Header .= '<' . $this->MailFrom . '>';
    if( isset( $this->MailCc ) ){
      $Header .= "\r\n" . $this->GetMailCc();
    }
    if( isset( $this->MailBCc ) ){
      $Header .= "\r\n" . $this->GetMailBCc();
    }
    $Header .= mb_convert_encoding( $this->ContentType, $this->MailStrCode, $this->Encode );
    return $Header;

  }

  public function MakeReturn( $ReturnMail ){

    if( MAILIER::CheckMailAddr( $ReturnMail ) ){
      $this->MailReturn = "-f" . $ReturnMail;
    } else {
      array_push( $this->ErrorMessage , "Mail address of return mail is wrong!" );
    }

  }

  protected function GetReturn(){

    if( isset( $this->MailReturn ) ){
      return $this->MailReturn;
    } else {
      return false;
    }

  }

  public function SendMail(){

    if( count( $this->ErrorMessage ) > 0 ){
      print_r( "<pre>" );print_r( $this->ErrorMessage );print_r( "</pre>" );
      exit();
    } else {
      if( $this->GetReturn() ){
        $sendMail = @mb_send_mail( $this->GetMailTo(), $this->GetSubject(), $this->GetBody(), $this->GetHeader(), $this->GetReturn() );
      } else {
        $sendMail = @mb_send_mail( $this->GetMailTo(), $this->GetSubject(), $this->GetBody(), $this->GetHeader() );
      }
    }
    if( $sendMail )
    {
      echo "Mail is sent successfully.";
    } else {
      echo "Error: Mail is not sent.";
    }

  }

  private static function CheckMailAddr( $MailAddr ){

    return filter_var( $MailAddr, FILTER_VALIDATE_EMAIL );

  }

}