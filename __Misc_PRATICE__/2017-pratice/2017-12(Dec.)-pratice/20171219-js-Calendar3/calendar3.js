/**************************************************
 * A class for dealing with calendar.
 **************************************************/
class CALENDAR{

  constructor(){

    this.curDate     = new Date();
    this.year        = this.curDate.getFullYear();
    this.month       = this.curDate.getMonth() + 1;
    this.day         = this.curDate.getDay();
    this.today       = this.curDate.getDate();
    this.weeknth     = [ "1w", "2w", "3w", "4w", "5w", "6w" ];
    this.day_of_week = [ "Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat" ];
    this.month_en    = { 1:"January",
                         2:"Feburary",
                         3:"March",
                         4:"April",
                         5:"May",
                         6:"June",
                         7:"July",
                         8:"August",
                         9:"September",
                        10:"October",
                        11:"November",
                        12:"December"
                       };

  }

  GetMonthTitle( M ){

    return this.month_en[M];

  }

  GetMonthTitleKey( title ){

    for( var key in this.month_en ){
      if( this.month_en[key] === title ){
        return key;
      }
    }

  }

  GetDaysInFeb( year ){

    let days_in_Feb = ( year % 4 == 0 && year % 100 != 0 ) ? 29 : 28;
    if( year % 400 == 0 ){ days_in_Feb = 29; }
    return days_in_Feb;

  }

  Cal_days_in_month( Y, M ){

    let cal_days_in_month = { 0:31,//for 1 - 1 = 0
                              1:31,
                              2:this.GetDaysInFeb( Y ),
                              3:31,
                              4:30,
                              5:31,
                              6:30,
                              7:31,
                              8:31,
                              9:30,
                              10:31,
                              11:30,
                              12:31
                            };
    return cal_days_in_month[M];

  }

  /**
   * Create table cells for day-of-week.
   */
  MakeCalendar( Y, M ){

    this.WriteCalHead();
    this.RecordYM( Y, M );
    this.WriteCalendar( Y, M );

  }//End of MakeCalendar

  SwitchCalendar( clickedSpanId ){

    let action   = clickedSpanId,
        curYear  = this.GetYear(),
        curMonth = this.GetMonth(),
        Y, M;
    document.getElementById( "day-of-week" ).lastChild.remove();
    $( "#day" ).find( "tr" ).remove();
    CALENDAR.DelRecord();
    if( action == "prev" ){
      switch( curMonth ){
        case 1:
          Y = curYear - 1;
          M = 12;
          break;
        default:
          Y = curYear;
          M = curMonth - 1;
          break;
      }
    } else {
      switch( curMonth ){
        case 12:
          Y = curYear + 1;
          M = 1;
          break;
        default:
          Y = curYear;
          M = curMonth + 1;
          break;
      }
    }
    this.MakeCalendar( Y, M );

  }// End of switch calendar

  WriteCalHead(){

    let thead = document.getElementById( "day-of-week" ),
        tr = ELEM.MakeHTMLElem( "tr" );
    for( var i = 0; i < 7; ++i ){
      let th = ELEM.MakeHTMLElem( "th" ),
          span = ELEM.MakeHTMLElem( "span" );
      span.innerHTML = this.day_of_week[i] + ".";
      if( i == 6 || i == 0 ){ span.style.color = "red"; }
      th.appendChild( span );
      tr.appendChild( th );
    }
    thead.appendChild( tr );

  }

  WriteCalendar( Y, M ){

    var tbody = document.getElementById( "day" ),
        first_day_this_month = new Date( Y, M - 1, 1 ).getDay(),
        last_day_this_month  = new Date( Y, M - 1, this.Cal_days_in_month( Y, M ) ).getDay(),
        days_prev_month = this.Cal_days_in_month( Y, M - 1 ),
        idYear  = ( ( M + 1 ) > 12 ) ? ( Y + 1 ) : ( Y ),
        idMONTH = ( ( M + 1 ) > 12 ) ? ( 1 ) : ( M + 1 );
    //for first week
    var tr = ELEM.MakeHTMLElem( "tr" );
    tr.id  = this.weeknth[0];
    for( var i = 0; i < 7; ++i ){
      var td   = ELEM.MakeHTMLElem( "td" ),
          span = ELEM.MakeHTMLElem( "span" ),
          day_in_prev_month = days_prev_month - ( first_day_this_month - 1 - i );
      if( i < first_day_this_month ){
        if( ( M - 1 ) == 0 ){
          span.id = ( Y - 1 ) + "-" + 12 + "-" + day_in_prev_month;
        } else {
          span.id = Y + "-" + ( M - 1 ) + "-" + day_in_prev_month;
        }
        span.style.color = "rgba(192,192,192,0.5)";
        span.innerHTML = day_in_prev_month;
      } else {
        span.id = Y + "-" + M + "-" + ( i - first_day_this_month + 1 );
        span.classList.add( "active" );
        span.innerHTML = ( i - first_day_this_month + 1 );
        if( i == 0 || i == 6 ){
          span.style.color = "red";
        }
        if( ( i - first_day_this_month + 1 ) == this.today && M == this.month && Y == this.year ){
          td.style.backgroundColor = "lightgreen";
        }
      }
      span.style.cursor = "pointer";
      td.appendChild( span );
      tr.appendChild( td );
    }
    tbody.appendChild( tr );
    //for second to fifth week
    for( var i = 1; i <= 4; ++i ){
      let tr_prev_week = document.getElementById( this.weeknth[i-1] ),
          lastElement  = Number( tr_prev_week.lastChild.lastChild.innerHTML ),
          tr = ELEM.MakeHTMLElem( "tr" );
      tr.id  = this.weeknth[i];
      for( var j = 0; j < 7; ++j ){
        var td   = ELEM.MakeHTMLElem( "td" );
            span = ELEM.MakeHTMLElem( "span" );
        if( ( lastElement + j + 1 ) <= this.Cal_days_in_month( Y, M ) ){
          span.id = Y + "-" + M + "-" + ( lastElement + j + 1 );
          span.classList.add( "active" );
          span.innerHTML = ( lastElement + j + 1 );
          if( j == 0 || j == 6 ){
            span.style.color = "red";
          }
          if( ( lastElement + j + 1 ) == this.today && M == this.month && Y == this.year ){
            td.style.backgroundColor = "lightyellow";
          }
        } else {
          span.id = idYear + "-" + idMONTH + "-" + ( lastElement + j + 1 - this.Cal_days_in_month( Y, M ) );
          span.style.color = "rgba(192,192,192,0.5)";
          span.innerHTML = ( lastElement + j + 1 - this.Cal_days_in_month( Y, M ) );
        }
        span.style.cursor = "pointer";
        td.appendChild( span );
        tr.appendChild( td );
      }
      let tbody_node = tr_prev_week.parentNode;
      tbody_node.insertBefore( tr, null );
    }
    //for the last week
    var tr_prev_week = document.getElementById( this.weeknth[4] ),
        lastElement  = Number( tr_prev_week.lastChild.lastChild.innerHTML ),
        tr_last_week = ELEM.MakeHTMLElem( "tr" ),
        record = 0;
    tr_last_week.id = this.weeknth[5];
    for( var i = 0; i < 7; ++i ){
      let td   = ELEM.MakeHTMLElem( "td" ),
          span = ELEM.MakeHTMLElem( "span" );
      if( lastElement < this.Cal_days_in_month( Y, M ) && lastElement > 10 ){
        if( i <= last_day_this_month ){
          span.id = Y + M + "-" + ( lastElement + i + 1 );
          span.classList.add( "active" );
          span.innerHTML = ( lastElement + i + 1 );
          if( i == 0 || i == 6 ){
            span.style.color = "red";
          }
          if( ( lastElement + i + 1 ) == this.today && M == this.month && Y == this.year ){
            span.style.backgroundColor = "lightgreen";
            span.style.borderRadius = "30px";
          }
          record++;
        } else {
          span.id = idYear + "-" + idMONTH + "-" + ( i - record + 1 );
          span.style.color = "rgba(192,192,192,0.5)";
          span.innerHTML = ( i - record + 1 );
        }
      } else {
        if( last_day_this_month != 6 ){
          span.id = idYear + "-" + idMONTH + "-" + ( lastElement + i + 1 );
          span.innerHTML = ( lastElement + i + 1 );
        } else {
          span.id = idYear + "-" + idMONTH + "-" + ( i + 1 );
          span.innerHTML = ( i + 1 );
        }
        span.style.color = "rgba(192,192,192,0.5)";
      }
      span.style.cursor = "pointer";
      td.appendChild( span );
      tr_last_week.appendChild( td );
    }
    let tbody_node = tr_prev_week.parentNode;
    tbody_node.insertBefore( tr_last_week, null );

  }

  GetYear(){

    let year = Number( $( "#year" ).text() );
    if( !year ){ year = this.year; }
    return year;

  }

  GetMonth(){

    let month = Number( this.GetMonthTitleKey( $( "#month" ).text() ) );
    if( !month ){ month = this.month; }
    return month;

  }

  RecordYM( Y, M ){

    $( "#year" ).text( Y );
    $( "#month" ).text( this.GetMonthTitle( M ) );

  }

  static DelRecord(){

    $( "#year" ).text( "" );
    $( "#month" ).text( "" );

  }

}//End of Class


/**************************************************
 * A class for dealing with html element via JS.
 **************************************************/
class ELEM{

  static MakeHTMLElem( elem ){

    return document.createElement( elem );

  }

}