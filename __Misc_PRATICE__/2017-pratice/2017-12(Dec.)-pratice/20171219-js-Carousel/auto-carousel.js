var slideIndex = 0;
showSlides();

function showSlides() {
    let slides = $( ".slideshow-container" ).children( "div" );
    for ( var i = 0; i < slides.length; ++i ) {
        if( i == ( slideIndex - 1 ) ){
          slides[i].style.display = "block";
        } else {
          slides[i].style.display = "none";
        }
    }
    slideIndex++;
    if ( slideIndex > slides.length ) { slideIndex = 1; }
    setTimeout( showSlides, 2000 ); // Change image every 2 seconds
}