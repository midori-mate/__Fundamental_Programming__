<?php

####long error message
// function checkNum( $number ){
//   if( $number > 2 ){
//     throw new Exception( "value must be less than 2" );
//   }
//   return true;
// }

// checkNum( 3 );

###short error message
// function checkNum( $number ){
//   if( $number > 2 ){
//     throw new Exception( "value must be less than 2" );
//   }
//   return true;
// }

// try{
//   checkNum( 3 );
// } catch( Exception $e ){
//   echo "Error: " . $e->getMessage();
// }

########################
try
{
    throw new Exception("Error Processing Request", -14.23);
}
catch (Exception $exception)
{
    /**
     * get the code you define for the error message.
     * the code must be integer.
     */
    $code = $exception->getCode();
    /**
     * get the error message you define
     */
    $mes = $exception->getMessage();
    if ($code < 0)
    {
        var_dump( $mes );
        var_dump( $code );
        exit();
    }
}