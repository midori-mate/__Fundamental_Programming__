<?php
$dsn = "mysql:host=localhost;dbname=supermarket;charset=utf8";
$user = "alan";
$pw = "alan1202";

$dbh = new PDO ($dsn,$user,$pw);

if (isset($_GET["page"])){
    $page = (int)$_GET["page"];
} else {
    $page = 1;
}

if ($page > 1){
    $start = ($page * 4) - 4;
} else {
    $start = 0;
}

$sql = "SELECT*FROM list WHERE goods_hizuke>(NOW()-INTERVAL 31 DAY) ORDER BY goods_status DESC,  goods_hizuke DESC LIMIT {$start}, 4";
$stmt = $dbh->query($sql);

$content_order = "";
foreach ($stmt as $row) {
    $g_sorts = h_order($row["goods_sorts"]);
    $g_name = h_order($row["goods_name"]);
    $g_maker = h_order($row["goods_maker"]);
    $g_price = number_format(h_order($row["goods_price"]));
    $g_amounts = h_order($row["goods_amounts"]);
    $g_total = number_format(h_order($row["total_price"]));
    $g_link = h_order($row["goods_link"]);
    $g_status = h_order($row["goods_status"]);
    $g_hizuke = h_order($row["goods_hizuke"]);
    $g_id = h_order($row["goods_id"]);
    $g_orderer = h_order($row["goods_orderer"]);

    if ($g_status == "unpaid"){
        $content_order .= "<tr>
                                <th width='5%'><span style='color: red'>{$g_sorts}</span></th>
                                <th width='20%'><span style='color: red'>{$g_name}</span></th>
                                <th width='15%'><span style='color: red'>{$g_maker}</span></th>
                                <th width='5%'><span style='color: red'>{$g_price}</span></th>
                                <th width='9%'><span style='color: red'>{$g_amounts}</span></th>
                                <th width='6%'><span style='color: red'>{$g_total}</span></th>
                                <th width='5%'><a href='{$g_link}'>Enter</a></th>
                                <th width='5%'><span style='color: red'>{$g_status}</span></th>
                                <th width='15%'><span style='color: red'>{$g_hizuke}</span></th>
                                <th width='5%'><input type='submit' id='order' name='admin_function' value='Order'>
                                               <input type='submit' id='delete' name='admin_function' value='Delete'>
                                               <input type='hidden' id='g_id' name='g_id' value='$g_id'></th>
                                <th width='10%'><span style='color: red'>{$g_orderer}</span></th>
                           </tr><br>";
    }
    else{
        $content_order .= "<tr>
                                <th width='5%'><span style='color: black'>{$g_sorts}</span></th>
                                <th width='20%'><span style='color: black'>{$g_name}</span></th>
                                <th width='15%'><span style='color: black'>{$g_maker}</span></th>
                                <th width='5%'><span style='color: black'>{$g_price}</span></th>
                                <th width='9%'><span style='color: black'>{$g_amounts}</span></th>
                                <th width='6%'><span style='color: black'>{$g_total}</span></th>
                                <th width='5%'><a href='{$g_link}'>Enter</a></th>
                                <th width='5%'><span style='color: black'>{$g_status}</span></th>
                                <th width='15%'><span style='color: black'>{$g_hizuke}</span></th>
                                <th width='5%'><span style='color: black'>Order Locked</span></th>
                                <th width='10%'><span style='color: black'>{$g_orderer}</span></th>
                          </tr><br>";
    }
}
$order_history = $content_order;
echo $order_history."<br>";

$sql_num = "SELECT COUNT(*) goods_id FROM list";
$stmt_num = $dbh->query($sql_num);
$page_num = $stmt_num->fetchColumn();

$pagination = ceil($page_num/4);

if ($page > 1){
    echo '<a href=\'order_page.php?page='.($page-1).'\')><-前へ </a>&ensp;';
}

for ($i=$page; $i<=$page+1;$i++){
    if ($i <= $pagination){
        echo "<a href='order_page.php?page=$i'>".$i."</a>&ensp;";
    }
}

if ($page < $pagination){
    echo '<a href=\'order_page.php?page='.($page+1).'\')>後へ-> </a>&ensp;';
}

function h_order($var){
    $a = htmlspecialchars($var,ENT_QUOTES);
    return $a;
}