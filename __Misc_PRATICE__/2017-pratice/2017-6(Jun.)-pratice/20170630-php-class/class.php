<?php
class A {
    const const_val = "ok";
    public $country1 = "America";
    public $country2 = "Japan";
    protected $apl = "can not be directly accessed outside CLASS";
    private $ap2 = "can not be directly accessed outside CLASS";

    function fly () {
        print "from $this->country1 to $this->country2"."<br/>";
        print $this->apl."<br/>";
        
        /*  this is tested for error_control_operator.php  **
        print $this->$ap2;*/

        print $this->ap2;
    }
}
/**********string replace pratice******************************************
$search  = array('A', 'B', 'C', 'D', 'G');
$replace = array('B', 'C', 'D', 'E', 'K');
$subject = 'C';

/*****The strings of $search invovled in $subject will be replaced by the strings of $replace*****/
//echo str_replace($search, $replace, $subject);

$a = new A;
//========the way to display a contant variable involved in class A======
echo $a::const_val."<br/>";
//========the way to display a public variable involved in class A=======
echo $a->country1."<br/>";
$a->fly();
