$(function(){
    $('div.narrowing option').bind("click",function(){
        var re = new RegExp($('select[id="narrowing"]').val());
        //each loop
        $('table[id="example1"]').children("tbody").children("tr").each(function(){
            var txt = $(this).find("td:eq(0)").html();
            
            if(txt.match(re) != null){
                $(this).show();
                $(this).addClass("shown");
            } else {
                $(this).removeClass("shown");
                $(this).hide();
            }
        });
        //----------------
    });
});

$(function(){
    $('div.narrowing option').bind("click",function(){
        var len = $("#example1 tbody tr[class='shown']").size();
        $('table[id="example1"]').each(function(){
            $("div.pager").remove();
            var currentPage = 0;
            var numPerPage = 2;
            var numRows = len;
            var $table = $(this);
            $table.bind('repaginate',function(){
                $table.find('tbody tr[class="shown"]').hide().slice(currentPage*numPerPage,(currentPage+1)*numPerPage).show();
            });
            $table.trigger('repaginate');
            var numPages = Math.ceil(numRows/numPerPage);
            var $pager = $('<div class="pager"></div>');
            for (var page = 0; page < numPages; page++) {
                $('<span class="page-number"></span>').text(page + 1).bind('click', {
                    newPage: page
                }, function(event) {
                    currentPage = event.data['newPage'];
                    $table.trigger('repaginate');
                    $(this).addClass('active').siblings().removeClass('active');
                }).appendTo($pager).addClass('clickable');
            }
            $pager.insertAfter($table).find('span.page-number:first').addClass('active');
        });
    });
});