var random_array = Array();
var peak_finding_result;
var peaks, troughs;


function random_num_generator () {
  peak_finding_result = {peaks: [], troughs: []};

  for (var i = 0; i < 10; i++) {
    random_array[i] = Math.floor( Math.random() * 100 + 1 );
  }

  var random_num = document.getElementById("random_num");
  random_num.textContent = "The random numbers are: " + random_array;
  peak_finding (random_array);
}

function peak_finding (random_array) {
  var current_num, prev_num, next_num;
  peaks = document.getElementById("peak");
  troughs = document.getElementById("trough");

  for (var j = 1; j < 8; j++) {
    current_num = random_array[j];
    prev_num    = random_array[j - 1];
    next_num    = random_array[j + 1];

    if (current_num > prev_num && current_num > next_num) {
      peak_finding_result.peaks.push(current_num);
    } else if (current_num < prev_num && current_num < next_num) {
      peak_finding_result.troughs.push(current_num);
    }
  }
  peaks.textContent = "Peaks are: " + peak_finding_result.peaks;
  troughs.textContent = "Troughs are: " + peak_finding_result.troughs;
}