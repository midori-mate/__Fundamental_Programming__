$(function () {
  //define variables
  var results = 0;
  var inputs = "";
  var inputNum;
  //store numbers between each operator
  var inputNumArr = [];
  //store operators
  var operatorInp = [];

  $( ".numbers" ).on( "click", function() {
    document.getElementById( "screen" ).innerHTML += $(this).val();
    inputs += $(this).val();
    inputNum = parseFloat( inputs );
    if ( inputNum > 999999999 ) {
      alert ( "This number is too large!" );
    }
  });

  $( "#add" ).on( "click", function() {
    inputNumArr.push( inputNum );
    document.getElementById( "screen" ).innerHTML = "";
    operatorInp.push( "add" );
    inputs = "";
  });

  $( "#minus" ).on( "click", function() {
    inputNumArr.push( inputNum );
    document.getElementById( "screen" ).innerHTML = "";
    operatorInp.push( "minus" );
    inputs = "";
  });

  $( "#times" ).on( "click", function() {
    inputNumArr.push( inputNum );
    document.getElementById( "screen" ).innerHTML = "";
    operatorInp.push( "times" );
    inputs = "";
  });

  $( "#divide" ).on( "click", function() {
    inputNumArr.push( inputNum );
    document.getElementById( "screen" ).innerHTML = "";
    operatorInp.push( "divide" );
    inputs = "";
  });

  $( "#equal" ).on( "click", function() {
    inputNumArr.push( inputNum );
    results = inputNumArr[0];
    for (var i = 0; i < operatorInp.length; i++) {
      switch ( operatorInp[i] ) {
        case "add":
          results += inputNumArr[ i + 1 ];
          break;
        case "minus":
          results -= inputNumArr[ i + 1 ];
          break;
        case "times":
          results *= inputNumArr[ i + 1 ];
          break;
        case "divide":
          results /= inputNumArr[ i + 1 ];
          break;
      }
    }
    document.getElementById( "screen" ).innerHTML = results;
    inputNumArr = [];
    inputs = "";
    inputNumArr.push( results );
  });

  $( "#AC" ).on( "click", function() {
    inputNumArr = [];
    inputs = "";
    operatorInp = [];
    results = 0;
    document.getElementById( "screen" ).innerHTML = "";
  });

  $( ".percent" ).on( "click", function() {
    document.getElementById( "screen" ).innerHTML = (inputNum / 100);
  });

  $( "#L" ).on( "click", function() {
    document.getElementById( "screen" ).innerHTML = "By Himegi";
  });

});