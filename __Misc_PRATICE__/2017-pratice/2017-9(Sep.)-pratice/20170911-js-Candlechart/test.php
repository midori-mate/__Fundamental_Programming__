<?php
//==============================================================================
// システム名    ビットコイン自動売買システム
//
// 概要          1分差分値取得CRON
//
// 注意事項      
//
// 2017.06.19    杉浦   新規作成
//==============================================================================

//----------------------------------------------------------
// 設定
//----------------------------------------------------------
set_time_limit(0);
date_default_timezone_set("Asia/Tokyo");
$dsn="mysql:host=localhost;dbname=bit;charset=utf8";
$user = "alan";
$pw = "alan1202";
$dbh = new PDO( $dsn, $user, $pw );
//==========================================================
// メイン処理
//==========================================================

//保存されたデータの最初の時間取得する
if ( !empty( SavedRecentDate( $dbh ) ) ){
    //今まで保存されたt_price_minuteの中に一番最近のデータ時間取得する
    $StartDataDate = SavedRecentDate( $dbh );
    $StartLoopNum = 1;
    var_dump("SavedRecentDate:  <br>" );
    var_dump($StartDataDate);
} else {
    //今まで保存されたw_priceの中に一番古いデータ時間取得する
    $StartDataDate = SavedDataDate( $dbh );
    $StartLoopNum = 0;
    var_dump("SavedRecentDate:  <br>" );
    var_dump($StartDataDate);
}
$StartYear   = Date( "Y", strtotime( $StartDataDate ) );
$StartMonth  = Date( "m", strtotime( $StartDataDate ) );
$StartDay    = Date( "d", strtotime( $StartDataDate ) );
$StartHour   = Date( "H", strtotime( $StartDataDate ) );
$StartMinute = Date( "i", strtotime( $StartDataDate ) );

//1分を取得する
for( $i = $StartLoopNum; $i < 10 ; ++$i )
{
    // 起動時間から1分毎に
    $checkTimeStart = Date('Y-m-d H:i:s', mktime( $StartHour, $StartMinute, ( $i * 60 ), $StartMonth, $StartDay, $StartYear ));
    $checkTimeEnd = Date('Y-m-d H:i:s', mktime( $StartHour, $StartMinute, ( ( $i + 1 ) * 60 ), $StartMonth, $StartDay, $StartYear ));
    // 直近の株価取得

    $sql = " SELECT";
    $sql.= "   A.`price_id` ";
    $sql.= "  ,A.`bitflyer_stocks_price` ";
    $sql.= "  ,A.`bitflyer_fx_price` ";
    $sql.= "  ,A.`coincheck_stocks_price` ";
    $sql.= "  ,A.`price_date` ";
    $sql.= " FROM";
    $sql.= "   `w_price` A ";
    $sql.= " WHERE";
    $sql.= "   price_date >= :price_date ";
    $sql.= "  AND ";
    $sql.= "   price_date < :price_date2 ";
    $sql.= " ORDER BY ";
    $sql.= "    A.price_id ASC ";
    $stmt = $dbh->prepare( $sql );
    $stmt->bindvalue( ":price_date", $checkTimeStart );
    $stmt->bindvalue( ":price_date2", $checkTimeEnd );
    $stmt->execute();
    $result = $stmt->setFetchMode( PDO::FETCH_ASSOC );
    $result = $stmt->fetchAll();
    if ( empty( $result ) ){
        echo( "No data in Table w_price " . $StartDataDate . " + " . $i . "minute" );
        continue;
    }
    var_dump( $result );
    // MinuteSave( $result, $checkTimeStart, $dsn, $db );
}
//------------------------関数---------------------------------------------
//一分間のデータを処理する
function MinuteSave( array $result, $checkTimeStart, $dsn, $dbh ){
    $LenOfData = count( $result );
    //各stock処理
    $bitflyer_stocks_price = array();
    $bitflyer_fx_price = array();
    $coincheck_stocks_price = array();
    for( $j = 0; $j < $LenOfData; ++$j ){
        $bitflyer_stocks_price[$j]  = $result[$j]["bitflyer_stocks_price"];
        $bitflyer_fx_price[$j]      = $result[$j]["bitflyer_fx_price"];
        $coincheck_stocks_price[$j] = $result[$j]["coincheck_stocks_price"];
    }
    //bitflyer_stocks_price
    $BitFlyerOpen   = $bitflyer_stocks_price[0];
    $BitFlyerClose  = $bitflyer_stocks_price[ $LenOfData - 1 ];
    $BitFlyerHigh   = max( $bitflyer_stocks_price );
    $BitFlyerLow    = min( $bitflyer_stocks_price );
    //bitflyer_FX_price
    $BitFlyerFXOpen  = $bitflyer_fx_price[0];
    $BitFlyerFXClose = $bitflyer_fx_price[ $LenOfData - 1 ];
    $BitFlyerFXHigh  = max( $bitflyer_fx_price );
    $BitFlyerFXLow   = min( $bitflyer_fx_price );
    //coincheck
    $CoinCheckOpen   = $coincheck_stocks_price[0];
    $CoinCheckClose  = $coincheck_stocks_price[ $LenOfData - 1 ];
    $CoinCheckHigh   = max( $coincheck_stocks_price );
    $CoinCheckLow    = min( $coincheck_stocks_price );
    
    $MinuteSave = array();
    //date
    $MinuteSave[]  = date( "YmdHi", strtotime( $checkTimeStart ) );
    //bitflyer
    $MinuteSave[] = $BitFlyerOpen;
    $MinuteSave[] = $BitFlyerHigh;
    $MinuteSave[] = $BitFlyerLow;
    $MinuteSave[] = $BitFlyerClose;
    //bitflyer FX
    $MinuteSave[] = $BitFlyerFXOpen;
    $MinuteSave[] = $BitFlyerFXHigh;
    $MinuteSave[] = $BitFlyerFXLow;
    $MinuteSave[] = $BitFlyerFXClose;
    //CoinCheck
    $MinuteSave[] = $CoinCheckOpen;
    $MinuteSave[] = $CoinCheckHigh;
    $MinuteSave[] = $CoinCheckLow;
    $MinuteSave[] = $CoinCheckClose;
    //price取得時間
    $MinuteSave[] = $result[0]["price_date"];
    // DBSave($MinuteSave, $dsn, $db); 
}
//DBにデータを保存する
function DBSave( $MinuteSave, $dsn, $dbh ){
    $setData = $MinuteSave;

    $sql = " INSERT INTO ";
    $sql.= " `t_price_minute` ";
    $sql.= " ( ";
    $sql.= " `price_key` ";
    $sql.= " ,`bitflyer_stocks_price_open` ";
    $sql.= " ,`bitflyer_stocks_price_high` ";
    $sql.= " ,`bitflyer_stocks_price_low` ";
    $sql.= " ,`bitflyer_stocks_price_close` ";
    $sql.= " ,`bitflyer_fx_price_open` ";
    $sql.= " ,`bitflyer_fx_price_high` ";
    $sql.= " ,`bitflyer_fx_price_low` ";
    $sql.= " ,`bitflyer_fx_price_close` ";
    $sql.= " ,`coincheck_stocks_price_open` ";
    $sql.= " ,`coincheck_stocks_price_high` ";
    $sql.= " ,`coincheck_stocks_price_low` ";
    $sql.= " ,`coincheck_stocks_price_close` ";
    $sql.= " ,`price_date` ";
    $sql.= " ) ";
    $sql.= " VALUES ( ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ) ";

    $stmt = $dbh->prepare( $sql );
    $stmt->bindvalue( ":price_date", $checkTimeEnd );
    $stmt->execute();
    $result = $stmt->setFetchMode( PDO::FETCH_ASSOC );
    $result = $stmt->fetchAll();
}
//今まで保存されたw_priceの中に一番古いデータ時間取得する
function SavedDataDate( $dbh ){
    $sql = " SELECT";
    $sql.= "  A.`price_date` ";
    $sql.= " FROM";
    $sql.= "   `w_price` A ";
    $sql.= " ORDER BY ";
    $sql.= " `price_date` ASC ";
    $sql.= " LIMIT 0, 1 ";
    $stmt = $dbh->query( $sql );
    $SavedDataDate = $stmt->setFetchMode( PDO::FETCH_ASSOC );
    $SavedDataDate = $stmt->fetchAll();
    return $SavedDataDate[0]["price_date"];
}
//今まで保存されたt_price_minuteの中に一番最近のデータ時間取得する
function SavedRecentDate( $dbh ){
    $sql = " SELECT";
    $sql.= "  A.`price_date` ";
    $sql.= " FROM";
    $sql.= "   `t_price_minute` A ";
    $sql.= " ORDER BY ";
    $sql.= " `price_date` DESC ";
    $sql.= " LIMIT 0, 1 ";
    $stmt = $dbh->query( $sql );
    $SavedRecentDate = $stmt->setFetchMode( PDO::FETCH_ASSOC );
    $SavedRecentDate = $stmt->fetchAll();
    return $SavedRecentDate[0]["price_date"];
}