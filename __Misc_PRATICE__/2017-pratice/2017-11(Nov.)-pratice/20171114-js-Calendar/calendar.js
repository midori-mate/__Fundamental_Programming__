/**
 * Define constants.
 */
var day_of_week = [ "Sun", "Mon", "Tue", "Wed", "Thur", "Fri", "Sat" ],
    week_array  = [ "1w", "2w", "3w", "4w", "5w", "6w" ],
    month_en = { 1:"January", 2:"Feburary", 3:"March", 4:"April", 5:"May", 6:"June", 7:"July", 8:"August", 9:"September",
                 10:"October", 11:"November", 12:"December" };

/**
 * Initialize setting of date
 */
var current = new Date(),
    year    = current.getFullYear(),
    month   = current.getMonth() + 1,
    day     = current.getDay(),
    today   = current.getDate(),
    cal_days_in_month,
    days_in_selected_month,
    days_in_Feb = ( year % 4 == 0 && year % 100 != 0 ) ? 29 : 28;
if( year % 400 == 0 ){ days_in_Feb = 29; }

cal_days_in_month = { 1:31, 2:days_in_Feb, 3:31, 4:30, 5:31, 6:30, 7:31, 8:31, 9:30, 10:31, 11:30, 12:31 };

window.onload = function(){
  MakeCalendar( month );
}

$( function() {

    $( "span" ).on( "click", function() {
        var span_id = $( this ).attr( "id" ),
            select_month = document.getElementById( "select_month" ).innerHTML;
        document.getElementById( "day-of-week" ).lastChild.remove();
        $( "#day" ).find( "tr" ).remove();
        $( "#select_month" ).text( "" );
        $( "#month" ).text( "" );
        ( span_id == "prev" ) ? MakeCalendar( Number( select_month ) - 1 ) : MakeCalendar( Number( select_month ) + 1 );

    });

});

function MakeCalendar( month ){

  /**
   * Create table cells for day-of-week.
   */
  $( function() {
      $( "#select_month" ).text( month );
      var thead = document.getElementById( "day-of-week" );
      $( "#month" ).text( year + "  " + month_en[month] );

      var tr = document.createElement( "tr" );
      for( var i = 0; i < day_of_week.length; ++i ){
        var th = document.createElement( "th" ),
            span = document.createElement( "span" );
        span.innerHTML = day_of_week[i] + ".";
        if( i == 6 || i == 0 ){ span.style.color = "red"; }
        th.appendChild( span );
        tr.appendChild( th );
      }
      thead.appendChild( tr );

  });
  /**
   * Current Month and days in selected month
   */
  var NowMonth = current.getMonth() + 1;
  days_in_selected_month   = cal_days_in_month[ month ];

  /**
   * Create table cells for days this month.
   */
  $( function() {

      var tbody = document.getElementById( "day" ),
          first_day_this_month = new Date( year, month - 1, 1 ).getDay(),
          last_day_this_month  = new Date( year, month - 1, days_in_selected_month ).getDay(),
          days_prev_month      = cal_days_in_month[ month - 1 ];

      //for first week
      var tr = document.createElement( "tr" );
      tr.id  = week_array[0];
      for( var i = 0; i < 7; ++i ){
        var td   = document.createElement( "td" ),
            span = document.createElement( "span" ),
            day_in_prev_month = days_prev_month - ( first_day_this_month - 1 - i );
        if( i < first_day_this_month ){
          span.id = ( month - 1 ) + "-" + day_in_prev_month;
          span.style.color = "rgba(192,192,192,0.5)";
          span.innerHTML = day_in_prev_month;
        } else {
          span.id = month + "-" + ( i - first_day_this_month + 1 );
          span.classList.add( "active" );
          span.innerHTML = ( i - first_day_this_month + 1 );
          if( i == 0 || i == 6 ){
            span.style.color = "red";
          }
          if( ( i - first_day_this_month + 1 ) == today && month == NowMonth ){
            span.style.backgroundColor = "lightgreen";
            span.style.borderRadius = "30px";
          }
        }
        span.style.cursor = "pointer";
        td.appendChild( span );
        tr.appendChild( td );
      }
      tbody.appendChild( tr );
      //for second to fiveth week
      for( var i = 1; i <= 4; ++i ){
        var tr_prev_week = document.getElementById( week_array[i-1] ),
            lastElement  = tr_prev_week.lastChild.lastChild.innerHTML,
            tr = document.createElement( "tr" );
        tr.id  = week_array[i];
        for( var j = 0; j < 7; ++j ){
          var td   = document.createElement( "td" );
              span = document.createElement( "span" );
          if( (Number( lastElement ) + j + 1) <= days_in_selected_month ){
            span.id = month + "-" + (Number( lastElement ) + j + 1);
            span.classList.add( "active" );
            span.innerHTML = (Number( lastElement ) + j + 1);
            if( j == 0 || j == 6 ){
              span.style.color = "red";
            }
            if( (Number( lastElement ) + j + 1) == today && month == NowMonth ){
              span.style.backgroundColor = "lightgreen";
              span.style.borderRadius = "30px";
            }
          } else {
            span.id = (month + 1) + "-" + (Number( lastElement ) + j + 1 - days_in_selected_month);
            span.style.color = "rgba(192,192,192,0.5)";
            span.innerHTML = (Number( lastElement ) + j + 1 - days_in_selected_month);
          }
          span.style.cursor = "pointer";
          td.appendChild( span );
          tr.appendChild( td );
        }
        var tbody_node = tr_prev_week.parentNode;
        tbody_node.insertBefore( tr, null );
      }
      //for the last week
      var tr_prev_week = document.getElementById( week_array[4] ),
          lastElement  = tr_prev_week.lastChild.lastChild.innerHTML,
          tr_last_week = document.createElement( "tr" ),
          record = 0;
      tr_last_week.id = week_array[5];
      for( var i = 0; i < 7; ++i ){
        var td   = document.createElement( "td" ),
            span = document.createElement( "span" );
        if( lastElement < days_in_selected_month && lastElement > 10 ){
          if( i <= last_day_this_month ){
            span.id = month + "-" + ( Number( lastElement ) + i + 1 );
            span.classList.add( "active" );
            span.innerHTML = ( Number( lastElement ) + i + 1 );
            if( i == 0 || i == 6 ){
              span.style.color = "red";
            }
            if( ( Number( lastElement ) + i + 1 ) == today && month == NowMonth ){
              span.style.backgroundColor = "lightgreen";
              span.style.borderRadius = "30px";
            }
            record++;
          } else {
            span.id = ( month + 1 ) + "-" + ( i - record + 1 );
            span.style.color = "rgba(192,192,192,0.5)";
            span.innerHTML = ( i - record + 1 );
          }
        } else {
          if( last_day_this_month != 6 ){
            span.id = ( month + 1 ) + "-" + ( Number( lastElement ) + i + 1 );
            span.style.color = "rgba(192,192,192,0.5)";
            span.innerHTML = ( Number( lastElement ) + i + 1 );
          } else {
            span.id = ( month + 1 ) + "-" + ( i + 1 );
            span.style.color = "rgba(192,192,192,0.5)";
            span.innerHTML = ( i + 1 );
          }
        }
        span.style.cursor = "pointer";
        td.appendChild( span );
        tr_last_week.appendChild( td );
      }
      var tbody_node = tr_prev_week.parentNode;
      tbody_node.insertBefore( tr_last_week, null );

  });

}