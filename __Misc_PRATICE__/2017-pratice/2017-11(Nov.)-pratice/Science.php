<?php
namespace Math;

class Statistic{


  protected $statisticData;

  function __construct( $statisticData ){

    $this->statisticData = $statisticData;

  }

  public function Standard_Deviation(){

    $n = count( $this->statisticData );
    $avg = array_sum( $this->statisticData ) / $n;
    $SDS = 0;
    foreach( $this->statisticData as $value ):
      $SDS += $value*$value - $avg*$avg;
    endforeach;

    return sqrt( $SDS / $n );

  }


}


namespace Physics;

class Dynamics{

  protected $g = 9.8;
  protected $velocity;
  protected $height;

  function __construct( $velocity, $height ){

    $this->velocity = $velocity;
    $this->height   = $height;

  }

  public function DynamicEnergyConservation(){


    if( !empty( $this->velocity ) && !empty( $this->height ) ){
      ( 1/2*$this->velocity*$this->velocity == $this->g * $this->height ) ? $conservation = "This is correct conservation result." : $conservation = "It never can derive this naturally.";
    } else {
      ( !empty( $this->height ) ) ? ( $conservation = "velocity = " . sqrt( 2 * g * $this->height ) . " (m/s) " ) : ( $conservation = "height = " . $this->velocity*$this->velocity/2/$this->g . " (m) " );
    }

    return $conservation;

  }


}
