<?php

class test{

  private $params;

  function __construct( $params ){

    $this->params = $params;

  }

  private function A(){

    echo "Here";

  }
  /**
   * This will be automatically executed at the end of file.
   */
  function __destruct(){

    $this->A();

  }

}

$result = new test( "apple" );
