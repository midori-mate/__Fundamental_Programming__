from sys import stdin, stdout
from operator import mul
from functools import reduce

NUM = 1000000007

def nCr(n,r):
  if r > 0:
    numer = reduce(mul,range(n-r+1,n+1))
    denom = reduce(mul,range(1,r+1))
    return numer//denom
  else:
    return 1

def FindAllSolutions( N ):
  res = 1
  MAXLEFT = int(N/2)
  for i in range(1,MAXLEFT+1):
    total = N+2*i
    res += nCr(total-1,i) - nCr(total-1,i-1)
  stdout.write( "%d\n" % ( res % NUM ) )

if __name__ == '__main__':
  FindAllSolutions(int(stdin.readline()))