################################################
##start to define class
# class test:
#   def __init__( self ):##-->construct
#     self.name   = "K"
#     self._name  = "k2"
#     self.__name = "k3"
# ##########################End###################
# test = test()
# print( test.name )###--->No error
# print( test._name )###--->No error
# # print( test.__name )###--->attribute error
# print( test._test__name )###--->No error
################################################

class test2:
  public = "public variable"
  def __init__( self, name ):
    self.name = name
  def getName( self ):
    return self.name

test2 = test2( "Himegi" )
print( test2.getName() )