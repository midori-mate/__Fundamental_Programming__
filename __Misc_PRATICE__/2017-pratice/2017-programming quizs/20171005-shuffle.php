<?php
// //----array_unique----
// $arr = array( "apple", "banana", "apple" );
// var_dump( array_unique($arr) );//------------>array( "apple", "banana" )

// //------shuffle-------
$nums = range( 1, 10 );
shuffle( $nums );
foreach ($nums as $num) {
  var_dump( $num );
}//----------------->random 1 to 10 nums forms an array