#!/usr/bin/env python
# coding: utf-8

'''トロンボーン
N×N のマス目で表される楽器庫があり、楽器庫には障害物がある。その楽器庫に1×(N−1) の長方形で表されるトロンボーンを X 個入れる。
トロンボーンは回転させることができるが、マス目に入ってなければならない、すなわちトロンボーンの辺がすべてマス目の区切りの部分に入っていなければならない。
また、トロンボーンは重なって置くことができず、楽器庫の障害物が置かれているマスがトロンボーンの一部分に入ってはいけません。
そのとき、すべてのトロンボーンが収納できるような最大の X を求めなさい。
'''

import itertools
from pprint import pprint

# 倉庫はこういう形式で用意されているものとします。
# TROMBONE_SIZE = 2
# WAREHOUSE = '''
# #..
# ...
# ..#
# '''
# TROMBONE_SIZE = 4
# WAREHOUSE = '''
# #.#.#
# .....
# #.#.#
# #.#.#
# .....
# '''
TROMBONE_SIZE = 4
WAREHOUSE = '''
.....
.....
.....
.....
.....
'''

# 通路部分座標を求めます。
def get_paths(warehouse):
    rows = filter(lambda _: _, warehouse.split('\n'))
    for y, row in enumerate(rows):
        for x, cell in enumerate(row):
            if cell == '.':
                yield (x, y)

# トロンボーンを置ける全パターンを出します。
def get_trombone_patterns(paths, trombone_size):
    for path in paths:
        for direction in ['up', 'down', 'left', 'right']:
            coors = get_trombone_coors(path, trombone_size, direction, paths)
            if coors:
                yield coors

# 基準座標から、上下左右方向にトロンボーン・サイズぶんの空きがあれば座標を返す。なければFalseを返します。
def get_trombone_coors(base_path, trombone_size, direction, paths):

    x, y = base_path
    trombone_coors = []
    can_be_placed = True
    for i in range(1, trombone_size):

        if direction == 'up':
            _ = (x, y-i)
        elif direction == 'down':
            _ = (x, y+i)
        elif direction == 'left':
            _ = (x-i, y)
        elif direction == 'right':
            _ = (x+i, y)

        if _ not in paths:
            can_be_placed = False
            break

        trombone_coors.append(_)

    if can_be_placed:
        trombone_coors.insert(0, base_path)
        return trombone_coors

    return False

# おんなじやつ(始点と終点が同じ)を削除します。
def remove_same_coors(coors):
    for coor in coors:
        point_a, point_b = coor[0], coor[-1]

        for i, coor_ in enumerate(coors):
            if point_a == coor_[-1] and point_b == coor_[0]:
                del coors[i]

# 全パターントロンボーン置いて、一番置けたときの個数と座標を返します。
def get_answer(paths):

    answer = 0
    answer_pattern = []

    for paths_copy in itertools.permutations(paths):

        paths_copy = list(paths_copy)

        trombone_num = 0
        this_pattern = []

        for coors in paths_copy:

            # いっこトロンボーンを置く。
            trombone_num += 1
            this_pattern.append(coors)

            # そのトロンボーンと同じ座標をもつものを削除する。
            remove_duplicated_coors(coors, paths_copy)

        # すべて置き終わったとき、最高記録を更新する。
        if trombone_num > answer:
            answer = trombone_num
            answer_pattern = this_pattern

    return (answer, answer_pattern)

# coorsが含む座標をもつものをpaths_copyから削除する。ただしcoors自身はスキップする。
def remove_duplicated_coors(target_coors, paths_copy):

    delete_indexes = []

    for i, coors in enumerate(paths_copy):
        if target_coors == coors:
            continue

        for target_coor in target_coors:

            if target_coor in coors:
                delete_indexes.append(i)

    delete_indexes = list(set(delete_indexes))
    delete_indexes.reverse()

    for delete_index in delete_indexes:
        del paths_copy[delete_index]

paths = list(get_paths(WAREHOUSE))
trombone_patterns = list(get_trombone_patterns(paths, TROMBONE_SIZE))
remove_same_coors(trombone_patterns)
answer, answer_pattern = get_answer(trombone_patterns)
print(answer)
pprint(answer_pattern)
