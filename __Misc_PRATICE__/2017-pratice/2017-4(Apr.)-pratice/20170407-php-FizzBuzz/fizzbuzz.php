<?php
//1から100までカウントアップする。
//数字が3で割り切れる時はFizzと出力する。
//数字が5で割り切れる時はBuzzと出力する。
//数字が3でも5でも割り切れる時はFizzBuzzと出力する。
//どの条件にも合致しない場合は数字をそのまま出力する。
//ex: 1,2,Fizz,4,Buzz......

/*for ($i=1; $i<=100; $i++){
    $a=3; 
    $b=5;
    $c=$a*$b;
    if ($i % $c==0){
        echo " FizzBuzz ";
    }elseif($i % $b==0){
        echo " Buzz ";
    }elseif($i % $a==0){
        echo " Fizz ";
    }else{
        echo $i." ";
    }
}*/

for ($i=2; $i<=100; $i+=2){
    $a=3; 
    $b=5;
    $c=$a*$b;
    if ($i % $c==0){
        echo " FizzBuzz ";
    }elseif($i % $b==0){
        echo " Buzz ";
    }elseif($i % $a==0){
        echo " Fizz ";
    }else{
        echo $i." ";
    }
}