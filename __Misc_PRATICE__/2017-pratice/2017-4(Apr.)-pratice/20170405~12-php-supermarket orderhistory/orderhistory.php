<?php
//database of user information
$dsn = 'mysql:host=localhost;dbname=supermarket;charset=utf8';
$user = 'alan';
$pw = 'alan1202';

$dbh = new PDO($dsn,$user,$pw);
//get all ordered data
$sql_history = "SELECT*FROM list";
$stmt = $dbh->query($sql_history);

$content_history = "";
foreach ($stmt as $row) {
    $history_g_sort = h_history($row["goods_sorts"]);
    $history_g_name = h_history($row["goods_name"]);
    $history_g_maker = h_history($row["goods_maker"]);
    $history_g_price = h_history($row["goods_price"]);
    $history_g_amounts = h_history($row["goods_amounts"]);
    $history_g_link = h_history($row["goods_link"]);
    $history_g_status = h_history($row["goods_status"]);
    $history_g_hizuke = h_history($row["goods_hizuke"]);
    $history_g_account = h_history($row["goods_orderer"]);

//check goods that are paid or unpaid
    if ($history_g_status == "unpaid"){
        $content_history .= "<tr>
                                <th width='5%'><span style='color: red'>{$history_g_sort}</span></th>
                                <th width='20%'><span style='color: red'>{$history_g_name}</span></th>
                                <th width='15%'><span style='color: red'>{$history_g_maker}</span></th>
                                <th width='5%'><span style='color: red'>{$history_g_price}</span></th>
                                <th width='15%'><span style='color: red'>{$history_g_amounts}</span></th>
                                <th width='10%'><a href='{$history_g_link}'><span style='color: red'>Enter</span></a></th>
                                <th width='5%'><span style='color: red'>{$history_g_status}</span></th>
                                <th width='15%'><span style='color: red'>{$history_g_hizuke}</span></th>
                                <th width='10%'><span style='color: red'>{$history_g_account}</span></th>
                             </tr>";
    }
    else{
        $content_history .= "<tr>
                                <th width='5%'>{$history_g_sort}</th>
                                <th width='20%'>{$history_g_name}</th>
                                <th width='15%'>{$history_g_maker}</th>
                                <th width='5%'>{$history_g_price}</th>
                                <th width='15%'>{$history_g_amounts}</th>
                                <th width='10%'><a href='{$history_g_link}'>Enter</a></th>
                                <th width='5%'>{$history_g_status}</th>
                                <th width='15%'>{$history_g_hizuke}</th>
                                <th width='10%'>{$history_g_account}</th>
                             </tr>";
    }
}
$order_history = $content_history;
print<<<HISTORY

<!--the way to display-->

<html>
    <head>
    </head>
        <meta charset="utf8">
        <style type="text/css">
            body {margin-left: auto; margin-right: auto; width: 90em; height: 90em; background-image: url(winter.jpg);
                    background-size: cover;}
            h1 {font-family: 'Times New Roman', Times, serif; padding: 10px; border: 10px double #123456; background-color: #bdbfc7;
                background-color: rgba(255,255,255,0.6); font-style: oblique;} 
            form#g_history {margin-bottom: 1.5em; padding: 20px; font-family: 'Times New Roman', Times, serif;}
            div.g_class {height: 50px; background-color: Lightyellow; padding: 5px;background-color: rgba(255,255,255,0.7); 
                            text-align: center; font-style: oblique; font-size: 1.5em; font-weight: bolder;}
            div.g_history {overflow: auto; height: 1000px; background-color: Lightyellow; padding: 5px;
                            background-color: rgba(255,255,255,0.7); text-align: center; font-style: oblique; font-size: 1em;
                            font-weight: bolder;}
            table {width: 100%; font-style: oblique; font-size: 1em;}
        </style>
    <body>
        <form id="g_history" name="g_history" method="POST">
            <h1 align="center"><span style="color: black; text-shadow: -4px -2px 0 ghostwhite">History of Ordered Goods</span></h1>
                <div id="g_class" class="g_class">
                    <table border="3px" cellpadding="5px" text-align="center">
                        <tr>
                            <th width="5%">Sort</th>
                            <th width="20%">Name</th>
                            <th width="15%">Maker</th>
                            <th width="5%">Price</th>
                            <th width="15%">Amounts</th>
                            <th width="10%">Link</th>
                            <th width="5%">Status</th>
                            <th width="15%">Update time</th>
                            <th width="10%">Orderer</th>
                        </tr>
                    </table>
                </div>
                <div id="g_history" class="g_history">
                    <table border="3px" cellpadding="5px" text-align="center">
HISTORY;
//display
                 print($order_history);

print <<<OUTPUT
                    </table>
                </div>
        </form>
    </body>
</html>
OUTPUT;
function h_history($a){
    $b = htmlspecialchars($a);
    return $b;
}