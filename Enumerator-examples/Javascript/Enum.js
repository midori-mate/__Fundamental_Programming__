/**
 * indices should be defined first.
 */
var EnumColor = { Blue:0, Green:1, Red:2 };

/**
 * Add new element to EnumColor
 */
Object.defineProperty( EnumColor, "Yellow", {value: 3} );

/**
 * get the index of specific element of EnumColor
 */
console.log( EnumColor.Yellow );

/**
 * get value of specific element of EnumColor
 */
console.log( getValue( 2 ) );


function getValue( key ){
  // Object.keys(EnumColor)---> get all keys
  return Object.keys(EnumColor)[key];
}