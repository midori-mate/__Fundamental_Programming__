#include <stdio.h>

/**
 * usage 1
 * MyCol is defined as the variable of Color
 */
// enum Color { Blue, Green, Red };
// enum MyCol;
/**
 * usage 2
 * MyCol is defined as the variable of Color
 */
enum Color { Blue, Green, Red } MyCol;

const char* EnumString[] = { "Blue", "Green", "Red" };

int main()
{
  enum Color MyCol;
  MyCol = Blue;
  printf("%d\n", MyCol ); //---> show the index of Blue

  /**
   * This is the only way to get the values of enum Color.
   */
  int length = sizeof(EnumString)/sizeof(char*);
  for( int i = 0; i < length; ++i ){
    printf("%s\n", EnumString[i] );
  }

  /**
   * Print all indices
   */
  for( int j = Blue; j <= Red; ++j ){
    printf("%d\n", j );
  }

  return 0;
}