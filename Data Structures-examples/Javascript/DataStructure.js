///////////////////////////////////////////////////////////
/// list
/////////////////////
let arr = [];
// add elements to the end of array
arr.push( "uno" );
arr.push( "dos" );
arr.push( "tres" );
arr.push( "cuatro" );
// add elements to the head of array
arr.unshift( "head" );
// add elements to a specific position and N elements after it will be deleted
let P = 1, N = 0;
arr.splice( P, N, 100 );
console.log( arr.join( ", " ) );

// the length of array
console.log( "The size of array is " + arr.length );

// remove an element from the end
arr.pop();
console.log( "End is deleted: " + arr.join( ", " ) );
// remove an element from the scratch
arr.shift();
console.log( "Head is deleted: " + arr.join( ", " ) );


let arr1 = [ 1, 2, 3, 4, 5 ];
let arr2 = [ 10, 20, 30, 40, 50 ];
let arr3;
// the arr1 will not change after two arrays concatenate
arr3 = arr1.concat( arr2 );
console.log( "arr3 = arr1 + arr2: " + arr3.join( ", " ) );

// the arr1 will change after two arrays concatenate
Array.prototype.push.apply( arr1, arr2 );
console.log( "arr1 after concatenation: " + arr1.join( ", " ) );

// get an element by its index
console.log( arr1[2] );

// get an index of specific element
console.log( arr1.indexOf( 30 ) );

// get the last index of the same elements
console.log( arr1.lastIndexOf( 3 ) );

// get elements by assigned range( x <= index < x+k )
console.log( arr1.slice( 1,4 ) );

// get maximum and minimum
console.log( "Max: " + Math.max.apply( Math, arr1 ) );
console.log( "Min: " + Math.min.apply( null, arr1 ) );

// remove all elements
//// usage 1
// arr1.length = 0;
//// usage 2
arr1 = [];
console.log( arr1.join( ", " ) );


///////////////////////////////////////////////////////////
/// Dictionary
/////////////////////
let dict = {};
// add elements to dict
// usage 1
dict["uno"] = 1;
dict["dos"] = 2;
dict["tres"] = 3;
dict["cuatro"] = 4;
// usage 2
dict.apple = 5;

// delete an element by its key
delete dict['apple'];

// display all elements
console.log( dict );

//get all keys and values
console.log( Object.keys( dict ) );
console.log( Object.values( dict ) );

// length of dict
console.log( "length: " + Object.keys( dict ).length );

// sort dict
var dict2 = [ { name: "bob", age : 1 }, { name: "alan", age : 3 } ];
console.log( dict2.sort( ( a, b ) => a.name < b.name ? -1 : 1 ) );

// delete all elements
dict2 = {};
console.log( " the length of dict2 after deleting: " + Object.keys( dict2 ).length );

// compare two dictionaries
let d1 = [ { name: "bob", age : 1 }, { name: "alan", age : 3 } ];
let d2 = [ { name: "alan", age : 3 }, { name: "bob", age : 1 } ];
let d3 = [ { name: "bob", age : 1 }, { name: "alan", age : 3 } ];

console.log( JSON.stringify( d1 ) === JSON.stringify( d3 ) );// ---> true
console.log( d1 == d3 ); //---> false


///////////////////////////////////////////////////////////
/// Map
/////////////////////
const map = new Map();
// add elements to map collection
map.set( "uno", { name: "bob", age : 18 } );
map.set( "dos", { name: "alan", age : 12 } );
map.set( "tres", { name: "cookie", age : 21 } );
map.set( "cuatro", { name: "dog", age : 15 } );

// get a specific element by its key
console.log( map.get( "uno" ) );

// size of map
console.log( map.size );

// print all entries
console.log( map.entries() );
// //MapIterator {
//   [ 'uno', { name: 'bob', age: 18 } ],
//   [ 'dos', { name: 'alan', age: 12 } ],
//   [ 'tres', { name: 'cookie', age: 21 } ],
//   [ 'cuatro', { name: 'dog', age: 15 } ] }

// check whether an element exists or not by its key
console.log( "tres exists: ?" + map.has( "tres" ) );

//delete an element by its key
console.log( map.delete( "dos" ) ); //-->return true if key exists

// print all elements
console.log( map );
// Map {
//   'uno' => { name: 'bob', age: 18 },
//   'tres' => { name: 'cookie', age: 21 },
//   'cuatro' => { name: 'dog', age: 15 } }

// delete all elements
map.clear();
console.log( map.size ); //--->0

