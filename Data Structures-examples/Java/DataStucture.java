// for using arraylist
import java.util.ArrayList;

// for using arrays: sort, equal, fill, binarySearch
import java.util.Arrays;
// for sorting in reversal way
import java.util.Collections;

//for using map( dictionary in Java )
import java.util.HashMap;
import java.util.Map;

// for using hashtable, treemap, linkedhashmap
import java.util.Hashtable;
import java.util.TreeMap;
import java.util.LinkedHashMap;

public class DataStucture{
  public static void main(String[] args) {
    ////////////////////////////////////////////
    // Array, ArrayList
    ////////////////////////////////////////////
    // declaration 1
    // type[] variable = {}
    int[] num1 = { 1, 2, 3, 4 };

    // declaration 2
    // type[] variable = new type[]{}
    int[] num2 = new int[]{ 4, 2, 1, 3 };
    // sort in ascending way
    Arrays.sort( num2 );

    // ref: https://www.javadrive.jp/start/arraylist/index1.html
    ArrayList<Integer> num1List = new ArrayList<Integer>();
    // add elements to list
    num1List.add( 10 );
    num1List.add( 5 );
    num1List.add( 50 );
    num1List.add( 25 );

    // sort in descending way
    // Collections seems only for arrayList
    Collections.sort( num1List, Collections.reverseOrder() );
    System.out.println( num1List );

    // get specific element by its index
    int numFromList = num1List.get( 3 );
    System.out.println( numFromList );

    // find a specific element whether exists or not
    System.out.println( num1List.contains( 5 ) );//---> return boolean value

    // count the size of list
    System.out.println( num1List.size() );

    // return the first index of the target elements
    System.out.println( num1List.indexOf( 50 ) );

    // update the element by its index
    System.out.println( "The third element of original list: " + num1List.get(2) );
    num1List.set( 2, 999 );
    System.out.println( "The third element of modified list: " + num1List.get(2) );

    // clear all elements of the list
    num1List.clear();
    System.out.println( num1List );//---> there is nothing can be output

    // check whether the list is empty or not
    System.out.println( num1List.isEmpty() ); //---> return boolean value

    ////////////////////////////////////////////
    // Map ( or dictionary in Java ? )
    ////////////////////////////////////////////
    Map< String, Integer > map = new HashMap< String, Integer >();
    map.put( "uno", 1 );
    map.put( "dos", 2 );
    map.put( "tres", 3 );
    map.put( "cuatro", 4 );

    Map< String, Integer > map2 = new HashMap< String, Integer >();
    // copy all current elements in map to map2
    map2.putAll( map );
    // replace a specific element by its key
    map2.replace( "dos", 22 );
    System.out.println( "This is map 2 : " + map2 );

    // count the size of list
    System.out.println( map.size() );

    // find a specific element whether exists or not by its key
    System.out.println( map.containsKey( "uno" ) );

    // find a specific element whether exists or not by its value
    System.out.println( map.containsValue( 2 ) );

    // get specific element by its index if not, return null
    System.out.println( map.get( "cuatro" ) );

    // return all keys
    System.out.println( map.keySet() );

    // return all values
    System.out.println( map.values() );

    // return keys and corresponding values
    System.out.println( map.entrySet() );

    // return hash code
    System.out.println( map.hashCode() );

    // remove an element by its key
    map.remove( "uno" );
    System.out.println( map );

    // clear all elements
    map.clear();
    System.out.println( map );

    ////////////////////////////////////////////
    // HashMap, Hashtable, TreeMap, LinkedHashMap
    ////////////////////////////////////////////
    Map< String, String > HM  = new HashMap();
    Map< String, String > HT  = new Hashtable();
    Map< String, String > TM  = new TreeMap();
    Map< String, String > LHM = new LinkedHashMap();

    setData( HM );
    setData( HT );
    setData( TM );
    setData( LHM );

    System.out.println( "HashMap: " + HM ); //---> {uno=1, dos=2, tres=4, cuatro=3}
    System.out.println( "HashTable: " + HT ); //---> {dos=2, uno=1, tres=4, cuatro=3}
    System.out.println( "TreeMap: " + TM );// ---> {cuatro=3, dos=2, tres=4, uno=1}
    System.out.println( "LinkedHashMap: " + LHM ); // ---> {uno=1, dos=2, cuatro=3, tres=4}

  }

  private static void setData( Map< String, String > map ){
    map.put( "uno", "1" );
    map.put( "dos", "2" );
    map.put( "cuatro", "3" );
    map.put( "tres", "4" );
  }


}