/**
 * For the usages of string and char class functions,
 * refer to string.cpp in programming folder.
 */
#include <iostream>
// for using dynamic array
#include <vector>
#include <algorithm>
// for using string
#include <string>

using namespace std;

int nums[] = {1,2,3};

char str[] = "ABC";
// this is different from C
char str1[] = { 'A', 'B', 'C' };

std::string s = { 'D', 'E', 'F' };


int main(int argc, char const *argv[])
{
  for( int i : nums ){
    cout << i << " ";
  }

  cout << endl;

  cout << str << endl;

  // the length of char array str1
  int length = sizeof( str1 )/sizeof( char );
  for( int i = 0; i < length; ++i ){
    cout << str1[i] << " ";
  }

  cout << endl;

  // the length of string array by string class
  int stringLen = s.size();
  for( int i = 0; i < stringLen; ++i ){
    cout << s[i] << " ";
  }
  cout << endl << s << endl; //---> output a string

  ///////////////////////////
  /// Dynamic Array
  ///////////////////////////
  std::vector<char> v = { 'P', 'K', 'G', 'U', 'Q' };
  // std::vector<char> v( 10, '$' );

  // the length of v
  std::size_t vLength = v.size();
  cout << "Dynamic v's length : " << vLength << endl;

  // get the first element
  cout << " First element of dynamic array: " << v.front() << endl;
  // get the last element
  cout << " Last element of dynamic array: " << v.back() << endl;
  // get the n-th element
  int n = 3;
  cout << " N-th element of dynamic array: " << v.at( n ) << endl;
  // add an element to end of array
  v.push_back( 'F' );
  cout << " Last element of dynamic array after adding: " << v.back() << endl;

  // remove a specific element by its index
  // v.erase( v.begin() + 1 ); //---> erase k
  // remove elements at range ( x <= i < x+n )
  v.erase( v.begin(), v.begin() + 2 ); //---> erase p k

  // foreach in algorithm header
  std::for_each( v.begin(), v.end(), [](char c){
    cout << c << " ";
  });

  // delete all element
  v.clear();

  cout << endl;

  // for bool
  std::vector<bool> vBool( 10, true );
  cout << "Boolean of dynamic array before flipping : " << vBool[2] << endl;
  // flip over the bit, if 1, turn to be 0, and vice versa.
  vBool[2].flip();
  cout << "Boolean of dynamic array : " << vBool[2] << endl;


  cout << endl;
  return 0;
}