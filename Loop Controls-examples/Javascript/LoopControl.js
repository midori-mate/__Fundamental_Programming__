let ArrNums = [ 1, 2, 3, 4, 5 ];
let ObjNums = { 0:1, 1:2, 2:3, 3:4, 4:5 };
let strings = [ "A", "B", "C", "D", "E" ];

/**
 * Non-object
 */
////////////////////
// For loop
////////////////////
for ( let i = 0; i < ArrNums.length; ++i ) {
  // it only can print data with string type.
  // console.log includes break line.
  process.stdout.write( ArrNums[i].toString() + ", " );
}

console.log( "" );

for( var i in ArrNums ){
  process.stdout.write( i.toString() );
}

console.log( "" );

for( var i of ArrNums ){
  process.stdout.write( i.toString() );
}

console.log( "" );

////////////////////
// while loop
////////////////////
let index = 0;
while( index < strings.length ){
  process.stdout.write( strings[index] + ", " );
  index++;
}

console.log( "" );

let index2 = 0;
do{
  process.stdout.write( strings[index2] + ", " );
  index2++;
}while( index2 < strings.length );

console.log( "" );

////////////////////
// foreach loop
////////////////////
ArrNums.forEach( function( key, value ){
  process.stdout.write( "key is " + key.toString() + ", val is " + value.toString() + "\n" );
});


/**
 * Object
 */
////////////////////
// For loop
////////////////////
for( var i in ObjNums ){
  process.stdout.write( "object number " + i.toString() + "\n" );
}

console.log("");

for( var i = 0; i < Object.keys(ObjNums).length; ++i ){
  process.stdout.write( "object number " + ObjNums[i].toString() + "\n" );
}

console.log("");


////////////////////
// while loop
////////////////////
let objIndex = 0;
while( objIndex < Object.keys(ObjNums).length ){
  process.stdout.write( "object number " + ObjNums[objIndex].toString() + "\n" );
  objIndex++;
}

console.log("");

let objIndex2 = 0;
do{
  console.log( "object number " + ObjNums[objIndex2] );
  objIndex2++;
}while( objIndex2 < Object.keys(ObjNums).length );

console.log("");

////////////////////
// foreach loop
////////////////////
Object.keys( ObjNums ).forEach( function( key ) {
  console.log( "key is " + key );
  console.log( "value is " + ObjNums[key] );
});





