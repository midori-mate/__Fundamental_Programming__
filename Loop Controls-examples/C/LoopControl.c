#include <stdio.h>


int main(int argc, char const *argv[])
{

  int numbers[] = { 1,2,3,4,5 };
  int numLength = sizeof( numbers )/sizeof( int );
  char* strs[] = { "A", "B", "C", "D", "E" };
  int strsLength = sizeof( strs )/sizeof( char* );

  ////////////////////
  // For loop
  ////////////////////
  for( int i = 0; i < numLength; ++i ){
    printf("%d, ", numbers[i] );
  }
  printf( "\n For loop End \n" );

  ////////////////////
  // while loop
  ////////////////////
  int index = 0;
  while( index < strsLength ){
    printf("%s, ", strs[index] );
    index++;
  }

  int index2 = 0;
  do{
    printf("%s, ", strs[index2] );
    index2++;
  }while( index2 < strsLength );

  printf( "\n While loop End \n" );

  ////////////////////
  // forEach loop
  // ---> c has no foreach loop, so it should be
  //      defined by users.
  ////////////////////


  return 0;
}