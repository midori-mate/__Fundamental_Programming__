using System;

namespace LoopControl{

  class Program{

    static void Main( string[] args ){

      int[] numbers = {1,2,3,4,5};

      string[] strs = { "A","B","C","D","E" };

      ////////////////////
      // For loop
      ////////////////////
      for( int i = 0; i < numbers.Length; ++i ){
        Console.Write( numbers[i] );
        Console.Write( ", " );
      }
      Console.WriteLine( "For Loop End" );

      ////////////////////
      // While loop
      ////////////////////
      int index = 0;
      while( index < strs.Length ){
        Console.Write( strs[index] );
        Console.Write( ", " );
        index++;
      }

      int index2 = 0;
      do{
        Console.Write( strs[index2] );
        Console.Write( ", " );
        index2++;
      }while( index2 < strs.Length );
      Console.WriteLine( "While Loop End" );

      ////////////////////
      // Foreach loop
      ////////////////////
      foreach( int num in numbers ){
        Console.Write( num );
        Console.Write( ", " );
      }
      Console.WriteLine( "Foreach Loop End" );

      Console.ReadKey();
    }

  }
}