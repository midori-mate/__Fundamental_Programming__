public class LoopControl{

  public static void main( String[] args ) {

    int[] numbers = { 1, 2, 3, 4 };
    int nLength = numbers.length;

    String[] strs = { "A", "B", "C", "D" };
    int sLength = strs.length;

    //////////////////////
    // For Loop
    //////////////////////
    /// usage 1
    for ( int i = 0 ; i < nLength ; ++i ) {
      System.out.println( numbers[i] );
    }
    /// usage 2
    for( String str: strs ){
      System.out.println( str );
    }

    //////////////////////
    // While Loop
    //////////////////////
    int index = 0;
    while( index < nLength ){
      System.out.print( numbers[index] );
      System.out.print( ", " );
      index++;
    }
    int index2 = 0;
    do{
      System.out.print( strs[index2] );
      System.out.print( ", " );
      index2++;
    }while( index2 < sLength );


  }
}