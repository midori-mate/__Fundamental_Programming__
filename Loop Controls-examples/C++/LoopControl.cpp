#include <iostream>
#include <vector>
#include <algorithm>
#include <string.h>

using namespace std;


int main(int argc, char const *argv[])
{
  int numbers[] = { 1, 2, 3, 4, 5 };
  int numLength = sizeof( numbers )/sizeof( int );

  // single quotaiton should be used.
  char strs[] = { 'A', 'B', 'C', 'D', 'E' };
  int strsLength = sizeof( strs )/sizeof( char );

  ////////////////////
  // For loop
  ////////////////////
  for( int i = 0; i < numLength; ++i ){
    cout << ", " << numbers[i];
  }

  cout << endl;

  for( int j = 0; j < strsLength; ++j ){
    cout << ", " << strs[j];
  }

  cout << endl;

  for( char k : strs  ){
    cout << ", " << k;
  }

  cout << endl;

  ////////////////////
  // while loop
  ////////////////////
  int index = 0;
  while( index < numLength ){
    cout << ", " << numbers[index];
    index++;
  }

  cout << endl;

  int index2 = 0;
  do{
    cout << ", " << strs[index2];
    index2++;
  }while( index2 < strsLength );

  cout << endl;

  ////////////////////
  // foreach loop
  // for_each -> <algorithm>
  // dynamic array -> <vector>
  ////////////////////
  std::vector<int> v = {1,2,3,4,5};
  std::for_each( v.begin(), v.end(), [](int i){
    cout << ", " << i;
  });

  cout << endl;

  std::for_each( std::begin(v), std::end(v), [](int p){
    cout << ", " << p;
  });

  cout << endl;

  // <string.h> is necessary
  const char* s = "ABCDE";
  std::for_each( s, s+strlen(s), [](char string) {
    cout << ", " << string;
  });


  return 0;
}