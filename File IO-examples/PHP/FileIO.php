<?php
/**
 * 
 */
date_default_timezone_set( "Asia/Tokyo" );
class File
{
  private $path;
  private $Files = array();

  function __construct( $arg )
  {
    $this->path = $arg;
  }
  //============== list all files under the directory ==========================
  public function ListAllEntriesInDir()
  {
    if( $handle = opendir( $this->path ) )
    {
      while ( false !== ( $entry = readdir( $handle ) ) )
      {
        if ( $entry != "." && $entry != ".." && is_file( $this->path . $entry ) )
        {
          array_push( $this->Files, $entry );
        }
      }
    }
  }

  public function ListAllEntriesInDirByScandir()
  {
    $entry = array_diff(scandir( $this->path, 1 ), array( ".",".." ));
    foreach( $entry as $key => $value )
    {
      if( is_file( $this->path . $value ) )
      {
        array_push( $this->Files, $value );
      }
    }
  }

  public function ListAllEntriesInDirByGlob()
  {
    $this->Files = str_replace("$this->path/", "", glob( "$this->path/{*.txt,*.img}", GLOB_BRACE ));
  }
  //============== Get all listed files ==========================
  public function GetFileList()
  {
    return $this->Files;
  }
  //============== Delete overdue files ==========================
  public function DeleteFewTimeAgoFile()
  {
    // 5 minutes ago
    $time = 5*60;
    $currentTime = date( "Y-m-d H:i:s", time() - $time );
    foreach( $this->Files as $file )
    {
      if( date( "Y-m-d H:i:s",filectime( $this->path . $file ) ) <= $currentTime )
      {
        unlink( $this->path . $file );
      }
    }
  }

  public function ReadFileOnly( $filename )
  {
    $file = fopen( $this->path."/" . $filename, "r" ) or die( "Unable to open" );
    $content = fread( $file, filesize( $this->path."/".$filename ) );
    var_dump( $content );
    fclose( $file );
  }

  public function ReadAndWrite( $filename )
  {
    $file = fopen( $this->path."/".$filename, "a+" ) or die( "Unable to open" );
    $txt = "Parent \n\r";
    fwrite( $file, $txt );
    fclose( $file );
  }

}

$f = new File( "../" );
$f->ListAllEntriesInDirByGlob();
$files = $f->GetFileList();
print_r($files);
$f->ReadFileOnly( $files[0] );
$f->ReadAndWrite( $files[0] );