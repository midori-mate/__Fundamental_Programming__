<?php
ob_start(function($buf){
     return mb_convert_encoding($buf, 'SHIFT_JIS', 'UTF-8');
});

date_default_timezone_set("Japan");

$time1 = microtime(true);

// $urls = array( "https://www.advanced-media.co.jp/ir/irnews/feed",
//                "https://www.advanced-media.co.jp/newsrelease/category/newsrelease/feed" );
$urls = array( "http://ir.takara-bio.co.jp/contentFeeds/content/news/ja/news_all/news_IR" );

$curl_arr = array();
$master = curl_multi_init();

for($i = 0; $i < count($urls); ++$i){
  $curl_arr[$i] = curl_init($urls[$i]);
  curl_setopt($curl_arr[$i], CURLOPT_RETURNTRANSFER, true);
  curl_multi_add_handle($master, $curl_arr[$i]);
}
// 複数リクエスト
do{ curl_multi_exec($master, $running); } while( $running>0 );

for($i = 0; $i<count($urls);++$i){
  $result=curl_multi_getcontent($curl_arr[$i]);
  $result=simplexml_load_string($result);
  print_r($result);
  echo "===============================================================";
}
$time2 = microtime(true);
$diff = explode(".",$time2-$time1);
var_dump( date("s",$diff[0]).".".$diff[1]."\r\n");