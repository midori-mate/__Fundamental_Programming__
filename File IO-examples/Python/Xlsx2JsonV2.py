import openpyxl as opxl
import json
from sys import stdin, stdout

from datetime import datetime, timedelta
import copy

def GetValueList( data ):
  return [[cell.value if cell.value else "" for cell in row] for row in data]

# def MakeValueDict( data):
def MakeValueDict( data,T ):
  Dict = []
  DictPush = Dict.append
  Keys = list(T.values())
  # Keys = ["SIIRESAKI_JUSHO_NO","HINMOKU_NO","ZAIKO_SURYO","SAKUSEI_DATE"]
  CompareKeys = list(T.keys())
  r = {key:idx for idx,key in enumerate(data[0]) if key in CompareKeys}
  # r = {key:idx for idx,key in enumerate(data[0]) if key in Keys}
  ColLen = len(Keys)
  for item in data[1:]:
    tempDict = {Keys[i]: str(item[r[Keys[i]]]) for i in range(ColLen)}

    # u = tempDict["SAKUSEI_DATE"]
    # y, m, d = int(u[0:4]), int(u[4:6]), int(u[6:8])
    # for k in range(180):
    #   date = (datetime(y,m,d)+timedelta(days=-k)).strftime("%Y%m%d")
    #   temp = copy.deepcopy(tempDict)
    #   temp["SAKUSEI_DATE"] = date
    #   DictPush(temp)

    DictPush(tempDict)
  return Dict

def PickUpFileName( filepath ):
  return filepath.split(".")

def TranCsvToJson( filepath, filename, T ):
  csv = open(filepath, 'r')
  data = [line.replace("\n","").replace("\"","").split(",") for line in csv.readlines()]
  DataDict = MakeValueDict(data)

  if len(DataDict):
    with open( filename + '.json', 'w', encoding='utf-8' ) as f:
      json.dump( DataDict, f, ensure_ascii=False, indent=2, separators=(',', ': ') )
    stdout.write( "csvからjsonへの変換成功です\n" )
  else:
    stdout.write( "No Data\n" )

def TranXlsxToJson( filepath, filename, T):
  wb = opxl.load_workbook( filepath )
  SavedData = GetValueList( wb['Sheet1'] )
  DataDict = MakeValueDict(SavedData, T)

  if len(DataDict):
    with open( filename + '.json', 'w', encoding='utf-8' ) as f:
      json.dump( DataDict, f, ensure_ascii=False, indent=2, separators=(',', ': ') )
    stdout.write( "xlsxからjsonへの変換成功です\n" )
  else:
    stdout.write( "No Data\n" )

if __name__ == '__main__':
  filepath = stdin.readline().replace("\n","")
  T = {
    
  }
  if filepath:
    try:
      fileInfo = PickUpFileName( filepath )
      filename = fileInfo[0]
      extension = fileInfo[1]
      if extension == "csv":
        TranCsvToJson( filepath, filename, T )
      else:
        TranXlsxToJson( filepath, filename, T)
    except Exception as e:
      stdout.write( "Error: ファイルが見つかりません\n%s" % e )


'''
発注明細累積
T = {
    "事業所No":'JIGYOSYO_NO',
    "BUの名称":'BU_NAME',
    "事業所部課":'JIGYOSYO_BUKA',
    "事業所拠点":'JIGYOSYO_KYOTEN',
    "仕入先住所No":'SIIRESAKI_JUSHO_NO',
    "仕入先名称":'SIIRESAKI_NM',
    "得意先住所No":'TOKUISAKI_JUSYO_NO',
    "得意先名称":'TOKUISAKI_NM',
    "オーダー日付":'ORDER_DATE',
    "オーダーNo":'ORDER_NO',
    "オーダータイプ":'ORDER_TYPE',
    "行No":'GYO_NO',
    "品目No":'HINMOKU_NO',
    "記述１":'KIJUTU1',
    "記述２":'KIJUTU2',
    "オーダー数量":'ORDER_SURYO',
    "単位原価":'TANI_GENKA',
    "外貨単位原価":'GAIKA_TANI_GENKA',
    "通貨":'TUKA',
    "未決済金額":'MIKESSAI_KINGAKU',
    "外貨未決済金額":'GAIKA_MIKESSAI_KINGAKU',
    "要求日付":'YOKYU_DATE',
    "約束納入日":'YAKUSOKU_NONYU_DATE',
    "販売予定年月":'HANBAI_YOTEI_YM',
    "事業所倉庫No":'JIGYOSHO_SOKO_NO',
    "事業所倉庫名称":'JIGYOSHO_SOKO_NM',
    "購買担当者No":'KOBAI_TANTO_NO',
    "購買担当者名称":'KOBAI_TANTO_NM',
    "作成日":'SAKUSEI_DATE',
    "リリース担当者コード":'RELEASE_TANTO_CD',
    "リリース担当者名":'RELEASE_TANTO_NM',
    "メモ":'MEMO',
    "メモ1":'MEMO1',
    "発注担当者コード":'HATYU_TANTO_CD',
    "発注担当者名":'HATYU_TANTO_NM',
    "為替レート":'KAWASE_RATE',
    "納入場所":'NONYUBASYO',
    "納入場所名称":'NONYUBASYO_NM',
    "支払条件":'SIHARAI_JYOKEN',
    "保管場所":'HOKANBASYO',
    "保管場所名称":'HOKANBASYO_NM',
    "正味発注価格":'SHOMI_HATYU_KAKAKU',
    "価格単位":'KAKAKU_TANI',
    "基本数量単位":'KIHON_SURYO_TANI',
    "販売予定確度":'HANBAI_YOTEI_KAKUDO',
    "明細備考":'MEISAI_BIKO',
    "照会備考":'SHOKAI_BIKO',
    "品目グループ":'HINMOKU_GROUP',
    "危険物":'KIKENBUTU',
    "毒劇物":'DOKUGEKIBUTU',
    "GP認定":'GP_NINTEI',
    "Rohs指定品":'ROHS_SITEIHIN',
    "エンドユーザー名":'ENDUSER_NM',
    "請求先名":'SEIKYUSAKI_NM',
    "ｴﾝﾄﾞﾕｰｻﾞｰNO":'ENDUSER_NO',
    "請求先":'SEIKYUSAKI',
    "チャージ率":'CHARGE_RATE',
    "会社コード":'KAISYA_CD',
    "更新日時":'KOSHIN_DATETIME',
    "発注単位":'HATYU_TANI',
  }

受注明細累積
T = {
    "事業所No":'JIGYOSYO_NO',
    "BUの名称":'BU_NAME',
    "事業所部課":'JIGYOSYO_BUKA',
    "事業所拠点":'JIGYOSYO_KYOTEN',
    "仕入先住所No":'SIIRESAKI_JUSHO_NO',
    "仕入先名称":'SIIRESAKI_NM',
    "得意先住所No":'TOKUISAKI_JUSYO_NO',
    "得意先名称":'TOKUISAKI_NM',
    "オーダー日付":'ORDER_DATE',
    "オーダーNo":'ORDER_NO',
    "オーダータイプ":'ORDER_TYPE',
    "得意先購買オーダー":'TOKUISAKI_KOBAI_ORDER',
    "品目No":'HINMOKU_NO',
    "記述１":'KIJUTU1',
    "記述２":'KIJUTU2',
    "得意先部品番号":'TOKUISAKI_BUHIN_NO',
    "オーダー数量":'ORDER_SURYO',
    "単価":'TANKA',
    "通貨":'TUKA',
    "合計金額":'GOKEI_KINGAKU',
    "外貨合計金額":'GAIKA_GOKEI_KINGAKU',
    "要求日付":'YOKYU_DATE',
    "約束納期":'YAKUSOKU_NOKI',
    "事業所倉庫No":'JIGYOSHO_SOKO_NO',
    "事業所倉庫名称":'JIGYOSHO_SOKO_NM',
    "コミッションコード１":'COMMITION_CD1',
    "コミッション名称":'COMMITION_NM',
    "作成日":'SAKUSEI_DATE',
    "取引入力者コード":'TORIHIKI_NYURYOKU_CD',
    "取引入力者":'TORIHIKI_NYURYOKU_NM',
    "LOTシリアルNO":'LOT_SERIAL_NO',
    "行No":'GYO_NO',
    "ピッキングリストNo":'PICKINGLIST_NO',
    "出荷先":'SHUKASAKI',
    "出荷先名称":'SHUKASAKI_NM',
    "明細カテゴリ":'MEISAI_CATEGORY',
    "明細カテゴリ名称":'MEISAI_CATEGORY_NM',
    "業務ステータス":'GYOMU_ST',
    "業務ステータス名称":'GYOMU_ST_NM',
    "エンドユーザ№":'ENDUSER_NO',
    "エンドユーザー名":'ENDUSER_NM',
    "販売単位":'HANBAI_TANI',
    "基本数量単位":'KIHON_SURYO_TANI',
    "明細備考":'MEISAI_BIKO',
    "照会備考":'SHOKAI_BIKO',
    "危険物":'KIKENBUTU',
    "毒劇物":'DOKUGEKIBUTU',
    "GP認定":'GP_NINTEI',
    "Rohs指定品":'ROHS_SITEIHIN',
    "GP保留":'GP_HORYU',
    "換算レート":'KANSAN_RATE',
    "配送指示行１":'HAISO_SIJI_GYO1',
    "配送指示行２":'HAISO_SIJI_GYO2',
    "受注実績数量":'JUTYU_JISSEKI_SURYO',
    "数量単位":'SURYO_TANI',
    "正味価格":'SHOMI_KAKAKU',
    "価格単位":'KAKAKU_TANI',
    "入出荷ステータス":'NYUSHUKKA_ST',
    "保管場所コード":'HOKANBASHO_CD',
    "保管場所名":'HOKANBASHO_NM',
  }

仕入明細累積
T = {
    "事業所No":'JIGYOSYO_NO',
    "BUの名称":'BU_NAME',
    "事業所部課":'JIGYOSYO_BUKA',
    "事業所拠点":'JIGYOSYO_KYOTEN',
    "仕入先住所No":'SIIRESAKI_JUSHO_NO',
    "仕入先名称":'SIIRESAKI_NM',
    "得意先住所No":'TOKUISAKI_JUSYO_NO',
    "得意先名称":'TOKUISAKI_NM',
    "オーダー日付":'ORDER_DATE',
    "オーダーNo":'ORDER_NO',
    "オーダータイプ":'ORDER_TYPE',
    "仕入先備考":'SIIRESAKI_BIKO',
    "品目No":'HINMOKU_NO',
    "記述１":'KIJUTU1',
    "記述２":'KIJUTU2',
    "通貨":'TUKA',
    "実際入荷日":'JISSAI_NYUKA_DATE',
    "入荷数量":'NYUKA_SURYO',
    "単位原価":'TANI_GENKA',
    "入荷金額":'NYUKA_KINGAKU',
    "入荷金額外貨":'NYUKA_KINGAKU_GAIKA',
    "販売予定年月":'HANBAI_YOTEI_YM',
    "事業所倉庫No":'JIGYOSHO_SOKO_NO',
    "事業所倉庫名称":'JIGYOSHO_SOKO_NM',
    "購買担当者No":'KOBAI_TANTO_NO',
    "購買担当者名称":'KOBAI_TANTO_NM',
    "作成日":'SAKUSEI_DATE',
    "発注担当者コード":'HATYU_TANTO_CD',
    "発注担当者名":'HATYU_TANTO_NM',
    "ロットNo":'LOT_NO',
    "メモ":'MEMO',
    "行No":'GYO_NO',
    "メモ１":'MEMO1',
    "保管場所No":'HOKANBASHO_NO',
    "保管場所名称":'HOKANBASHO_NM',
    "為替レート":'KAWASE_RATE',
    "仕入金額(会社通貨額)":'SIIRE_KINGAKU_KAISYA',
    "仕入金額(国内通貨額)":'SIIRE_KINGAKU_KOKUNAI',
    "基本数量単位":'KIHON_SURYO_TANI',
    "プラント":'PURANTO',
    "外部番号":'GAIBU_NO',
    "販売予定確度":'HANBAI_YOTEI_KAKUDO',
    "明細備考":'MEISAI_BIKO',
    "照会備考":'SHOKAI_BIKO',
    "品目グループ":'HINMOKU_GROUP',
    "エンドユーザー名":'ENDUSER_NM',
    "請求先名":'SEIKYUSAKI_NM',
    "納入先名":'NOUNYUSAKI_NM',
    "明細":'MEISAI',
    "支払条件":'SIHARAI_JYOKEN',
    "エンドユーザーNo":'ENDUSER_NO',
    "請求先":'SEIKYUSAKI',
    "納入先":'NOUNYUSAKI',
    "チャージ率":'CHARGE_RITU',
    "危険物":'KIKENBUTU',
    "毒劇物":'DOKUGEKIBUTU',
    "GP認定":'GP_NINTEI',
    "Rohs指定品":'ROHS_SITEIHIN',
    "発注単位":'HATYU_TANI',
  }

売上原価明細累積
T = {
    "事業所No":'JIGYOSYO_NO',
    "BUの名称":'BU_NAME',
    "事業所部課":'JIGYOSYO_BUKA',
    "事業所拠点":'JIGYOSYO_KYOTEN',
    "仕入先住所No":'SIIRESAKI_JUSHO_NO',
    "仕入先名称":'SIIRESAKI_NM',
    "得意先住所No":'TOKUISAKI_JUSYO_NO',
    "得意先名称":'TOKUISAKI_NM',
    "オーダーNo":'ORDER_NO',
    "オーダータイプ":'ORDER_TYPE',
    "得意先購買オーダー":'TOKUISAKI_KOBAI_ORDER',
    "品目No":'HINMOKU_NO',
    "記述１":'KIJUTU1',
    "記述２":'KIJUTU2',
    "得意先部品番号":'TOKUISAKI_BUHIN_NO',
    "出荷数量":'SHUKA_SURYO',
    "単価":'TANKA',
    "通貨":'TUKA',
    "合計金額":'GOKEI_KINGAKU',
    "外貨合計金額":'GAIKA_GOKEI_KINGAKU',
    "円貨原価額":'ENKA_GENKAGAKU',
    "FOB円貨原価額":'FOB_ENKA_GENKAGAKU',
    "売上の仕入単価":'URIAGE_SIIRE_TANKA',
    "元帳日付":'MOTOTYO_DATE',
    "事業所倉庫No":'JIGYOSHO_SOKO_NO',
    "事業所倉庫名称":'JIGYOSHO_SOKO_NM',
    "コミッションコード１":'COMMITION_CD1',
    "コミッション名称":'COMMITION_NM',
    "作成日":'SAKUSEI_DATE',
    "取引入力者コード":'TORIHIKI_NYURYOKU_CD',
    "取引入力者名":'TORIHIKI_NYURYOKU_NM',
    "ロットNo":'LOT_NO',
    "ピッキングリストNo":'PICKINGLIST_NO',
    "請求書日付":'SEIKYUSHO_DATE',
    "行No":'GYO_NO',
    "出荷先":'SHUKASAKI',
    "出荷先名称":'SHUKASAKI_NM',
    "保管場所コード":'HOKANBASHO_CD',
    "保管場所名":'HOKANBASHO_NM',
    "販売単位":'HANBAI_TANI',
    "基本数量単位":'KIHON_SURYO_TANI',
  }
'''