import os

class File():
  def __init__(self, arg):
    self.dir = os.path.dirname( os.path.realpath(__file__) )
    #### Simple way
    # self.filepath = self.dir + "/../" + arg
    #### Join way
    self.filepath = os.path.join( self.dir, "../" + arg )
    self.file = None

  def ListAllFilesInDir( self ):
    return [ f for f in os.listdir( self.dir ) if os.path.isfile( os.path.join( self.dir, f ) ) ]

  def ReadOnly( self ):
    self.file = open( self.filepath, 'r' )

  def ReadAndWrite( self ):
    self.file = open( self.filepath, 'a+' )
    string = "Do Something\n\r"
    self.file.write( string )

  def GetFileContent( self ):
    return self.file.read()

  def CloseFile( self ):
    self.file.close()

f = File( "test1.txt" )
f.ReadAndWrite()
# f.ReadOnly()
# print( f.GetFileContent() )
# f.CloseFile()
# print( f.ListAllFilesInDir() )