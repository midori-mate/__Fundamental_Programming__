<?php
date_default_timezone_set( "Japan" );

$date = date( "Y-m-d H:i:s" );
var_dump( $date );
// get year
var_dump( "year: " . date( "Y" ) );
// get month
var_dump( "month: " . date( "m" ) );
// get day
var_dump( "day: " . date( "d" ) );
// get day name
var_dump( "day name: " . date( "D" ) );
// get hour
var_dump( "hour: " . date( "H" ) );
// get minute
var_dump( "minute: " . date( "i" ) );
// get second
var_dump( "second: " . date( "s" ) );

///// Measure Execution Time
$start = date_create( date( "Y-m-d H:i:s" ) );
// sleep in in unit of second
sleep(1);
$end = date_create( date( "Y-m-d H:i:s" ) );
$diff = date_diff( $end, $start );
var_dump( $diff->s );




