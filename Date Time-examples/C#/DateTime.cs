using System;

namespace DateTime{
  class Program{
    public static void Main( string[] args ){
      System.DateTime date = System.DateTime.Now;
      // default: yyyy/MM/dd HH:mm:ss
      Console.WriteLine( date );

      // format datetime
      Console.WriteLine( date.ToString( "yyyy-MM-dd HH:mm:ss" ) );

      // get Year
      Console.WriteLine( "Year: {0}", date.Year );
      // get Month
      Console.WriteLine( "Month: {0}", date.Month );
      // get Day
      Console.WriteLine( "Day: {0}", date.Day );
      // get Hour
      Console.WriteLine( "Hour: {0}", date.Hour );
      // get Minute
      Console.WriteLine( "Minute: {0}", date.Minute );
      // get Second
      Console.WriteLine( "Second: {0}", date.Second );
      // get Millisecond
      Console.WriteLine( "Millisecond: {0}", date.Millisecond );

      ///// Measure Execution Time
      var stopwatch = System.Diagnostics.Stopwatch.StartNew();
      System.Threading.Thread.Sleep( 1000 );
      stopwatch.Stop();
      Console.WriteLine( "Elapsed Time : {0}", stopwatch.ElapsedMilliseconds );


      Console.ReadKey();
    }
  }
}