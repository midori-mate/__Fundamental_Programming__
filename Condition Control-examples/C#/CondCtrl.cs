using System;

namespace CondCtrl{
  public class CC{
    public static void Main( string[] args ){
      const int num = 256;
      string str = "num is ";
      string str2 = "";

      /////////////////////////
      /// nested if control
      /////////////////////////
      if( num > 512 ){
        /// There is an error if using the way to concatenate two strings:
        /// CondCtrl.cs(14,9): warning CS0162: 到達できないコードが検出されました。
        str2 = "larger than 512";
      } else {
        if( num > 128 ){
          str2 = "larger than 128";
        } else {
          str2 = "less than 128";
        }
      }
      Console.WriteLine( str + str2 );

      /////////////////////////
      /// ternary operator
      /////////////////////////
      string str6 = ( num > 128 ) ? "larger than 128" : "less than 128";
      Console.WriteLine( str6 );

      /////////////////////////
      /// switch control
      /// The expression used in a switch statement must
      /// have an integral or enumerated type, or be of a
      /// class type in which the class has a single conversion
      /// function to an integral or enumerated type.
      /////////////////////////
      string str3;
      switch( num ){
        case 128:
          str3 = "128.";
          break;
        case 256:
          str3 = "the fave number of hacker.";
          break;
        case 512:
          str3 = "512.";
          break;
        default:
          str3 = "out of conditions.";
          break;
      }
      Console.WriteLine( str + str3 );


      Console.ReadKey();
    }
  }
}