public class CondCtrl{
  public static void main(String[] args) {
    int input = 20;
    String res = "Input is ";

    /////////////////////////
    /// nested if control
    /////////////////////////

    if( input < 20 ){
      System.out.println( res + "less than 20." );
    } else if( input > 10 ){
      System.out.println( res + "larger than 10." );
    } else {
      System.out.println( res + " >= 20 or <= 10." );
    }

    /////////////////////////
    /// ternary operator
    /////////////////////////
    String result = ( input < 20 ) ? "less than 20" : "larger than 20";
    System.out.println( result );

    /////////////////////////
    /// switch control
    /// The variable used in a switch statement
    /// can only be integers, convertable integers
    /// (byte, short, char), strings and enums.
    /////////////////////////
    int dbNum = 200;
    String res2 = "dbNum is ";

    switch( dbNum ){
      case 300:
        System.out.println( res2 + "= 300." );
        break;
      case 200:
        System.out.println( res2 + "= 200." );
        break;
      case 100:
        System.out.println( res2 + "= 100." );
        break;
      default:
        System.out.println( res2 + "out of conditions." );
        break;
    }

  }
}