num = 200.34
res = "num is "

#########################
### nested if control
#########################
if num > 300:
  res += "larger than 300."
else:
  if num > 200:
    res += "larger than 200."
  else:
    res += "less than 200."

print( res )

res2 = "num is "
if num > 300:
  res2 += "larger than 300."
elif num > 200:
  res2 += "larger than 200."
else:
  res2 += "less than 200."

print( res2 )


#########################
### ternary operator
#########################
res3 = "larger than 200" if num > 200 else "less than 200"
print( res3 )

#########################
### switch control
# It does not have this kind of switch control.
#########################


