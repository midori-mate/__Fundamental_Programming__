let num = 300.45
let str = "num is ";

/////////////////////////
/// nested if control
/////////////////////////
if( num > 400 ){
  str += "larger than 400.";
} else {
  if( num > 300 ){
    str += "larger than 300.";
  } else {
    str += "less than 300.";
  }
}

console.log( str );


let str2 = "num is ";

if( num > 400 ){
  str2 += "larger than 400.";
} else if( num > 300 ){
  str2 += "larger than 300.";
} else {
  str2 += "less than 300.";
}

console.log( str2 );

/////////////////////////
/// ternary operator
/////////////////////////
let str4 = ( num > 300 ) ? "larger than 300" : "less than 300";
console.log( str4 );

/////////////////////////
/// switch control
/////////////////////////
let str3 = "num is ";

switch( num ){
  case 200:
    str3 += num;
    break;
  case 300.45:
    str3 += num;
    break;
  case 100:
    str3 += num;
    break;
  default:
    str3 += "out of conditions.";
}

console.log( str3 );
