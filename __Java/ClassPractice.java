/**
 * Fundamental usages of class
 */
public class ClassPractice{

  // This is the definition of constant in java.
  // Java does not have keyword constant.
  private static final int Birth = 10;

  int Age;

  public ClassPractice( String name ){
    System.out.println( "My name is " + name );
  }

  public void setAge( int age ){
    Age = age + Birth;
  }

  public int getAge(){
    return Age;
  }

  public static void main( String []args ){
    ClassPractice cp = new ClassPractice( "Alan" );
    // set parameter
    cp.setAge( 18 );

    System.out.println( "Alan's age is " + cp.getAge() );
  }
}