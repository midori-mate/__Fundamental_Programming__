/**
 * iostream
 * Defines the cin, cout, cerr, clog objects.
 */
#include <iostream>
/**
 * iomainip
 * Defines the setw and setprecision
 */
// #include <iomainip>


using namespace std;

int main(){
  char something[50];

  // check version
  cout << __cplusplus << endl;

  cout << " Enter something " << endl;
  cin >> something;
  cout << "Something is: " << something << endl;

  int num;
  cin >> num;
  cout << "Number is " << num << endl;

  //----------------cerr----------------------------
  //char str[] = " Something is wrong... ";
  //cerr << " Error Message: " << str << endl;

  //----------------clog----------------------------
  //char str[] = " Something is log... ";
  //clog << " log: " << str << endl;

}