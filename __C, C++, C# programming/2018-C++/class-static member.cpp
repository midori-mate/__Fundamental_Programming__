#include <iostream>

using namespace std;

class MyStatic
{
  public:
    static int count;
    MyStatic( int x, int y ){
      x1 = x;
      y1 = y;
      count++;
    };
    ~MyStatic();
  private:
    int x1;
    int y1;
};


MyStatic::~MyStatic(){

}

int MyStatic::count = 0;

int main(){

  MyStatic s( 100,2 );
  MyStatic s2( 200,2 );

  cout << " Total objects are " << MyStatic::count << endl;

  return 0;
}