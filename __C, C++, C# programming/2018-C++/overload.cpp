#include <iostream>

using namespace std;

/**
 * Overload function declarations cannot differ only
 * by return type.
 * ---> The types of parameters must be different.
 */

class printGroup{

public:
  void printOverload( int i ){
    cout << " int : ";
    OutPut(i);
  }

  void printOverload( double i ){
    cout << " double : ";
    OutPut(i);
  }

  void printOverload( float i ){
    cout << " float : ";
    OutPut(i);
  }

private:
  void OutPut( double x ){
    cout << "  " << x << endl;
  }

};



int main(){

  printGroup pG;

  pG.printOverload(10);
  pG.printOverload(10.21);
  pG.printOverload(22.22);

  return 0;
}