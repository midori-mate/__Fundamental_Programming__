#include <iostream>
#include <cstring>

using namespace std;

struct Lists{
  int id;
  char name[10];
  int phone;
};

void printList( struct Lists list );
// pointer version
void printList2( struct Lists *list );

int main(){

  /**
   * Declaration of c++
   * For c, it will be [struct Lists one].
   */
  Lists one;
  one.id = 1;
  strcpy( one.name, "apple" );
  one.phone = 12304;

  Lists two;
  two.id = 3;
  strcpy( two.name, "appleapplea" );
  two.phone = 54321;

  printList( one );

  cout << "=====================" << endl;

  printList( two );

  cout << "======Pointer===============" << endl;

  printList2( &one );

  cout << "=====================" << endl;

  printList2( &two );

  return 0;
}

/**
 * struct can be used as parameter of function
 * @param list [description]
 */
void printList( struct Lists list ){
  cout << " This id is: " << list.id << endl;
  cout << " This name is: " << list.name << endl;
  cout << " This phone is: " << list.phone << endl;
}

/**
 * Pointer version
 * struct can be used as parameter of function
 * @param list [description]
 */
void printList2( struct Lists *list ){
  cout << " This id is: " << list->id << endl;
  cout << " This name is: " << list->name << endl;
  cout << " This phone is: " << list->phone << endl;
}