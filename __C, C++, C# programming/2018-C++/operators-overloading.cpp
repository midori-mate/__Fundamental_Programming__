#include <iostream>

using namespace std;

class sup{

  public:
    double CalVolume( void ){
      return length * height * width;
    }

    void setLength( double l ){
      length = l;
    }

    void setHeight( double h ){
      height = h;
    }

    void setWidth( double w ){
      width = w;
    }

    /**
     * Overload operator+ to add two objects sup
     */
    sup operator+( const sup& s ){
      sup s2;
      s2.length = this->length + s.length;
      s2.height = this->height + s.height;
      s2.width = this->width + s.width;
      return s2;
    }

  private:
    double length;
    double height;
    double width;

};

int main(){
  sup s1;
  sup s2;
  sup s3;

  double vol = 0;

  s1.setWidth( 10 );
  s1.setHeight( 20 );
  s1.setLength( 30 );

  s2.setWidth( 40 );
  s2.setHeight( 50 );
  s2.setLength( 60 );

  s3 = s1 + s2;

  vol = s1.CalVolume();
  cout << " s1 vol = " << vol << endl;
  vol = s2.CalVolume();
  cout << " s2 vol = " << vol << endl;
  vol = s3.CalVolume();
  cout << " s3 vol = " << vol << endl;
  cout << " Something else " << 3+4 << endl;

  return 0;
}