#include <iostream>
#include <typeinfo>
#include <vector>
#include <algorithm>
#include <string.h>

using namespace std;

// show types
void ShowTypes(){
  cout << "Size of char => " << sizeof( char ) << endl;
  cout << "Size of int => " << sizeof( int ) << endl;
  cout << "Size of unsigned int => " << sizeof( unsigned int ) << endl;
  cout << "Size of short int => " << sizeof( short int ) << endl;
  cout << "Size of long int => " << sizeof( long int ) << endl;
  cout << "Size of double => " << sizeof( double ) << endl;
  cout << "Size of wchar_t => " << sizeof( wchar_t ) << endl;
  cout << "Size of float => " << sizeof( float ) << endl;
}
// define a variable name as a type
void DefTypeWithNeeName(){
  typedef long cat;
  // here it needs typeinfo library to use typeid
  cout << " Type of cat is " << typeid(cat).name() << endl;
}

// Enumeration
void Enumerate(){
  // general statement
  // enum enum-name { list of names } var-list
  // alan => 0, apple => 10, blue => 11
  enum name { alan, apple = 10, blue };
  cout << name::blue << endl;
}

// define a constant
#define HEIGHT 10
#define WIDTH 100.0
void Area();

//static variable
// static int count = 10;
// void dowhile();

//do foreach
void ForWithDynamicArray();
void ForEach();

//conditions
void Conditions( int i );



/////////////////////////////
// above: function declarations
/////////////////////////////

/* here is execution */
int main(){
  // cout << "Hello World" << endl;
  ShowTypes();
  // DefTypeWithNeeName();
  // Enumerate();
  // Area();
  // dowhile();
  // ForWithDynamicArray();
  // ForEach();
  // Conditions(1);

  return 0;
}

/////////////////////////////
// below: function definitions
/////////////////////////////
void Area(){
  cout << HEIGHT * WIDTH << endl;
}

// void dowhile(){
//   static int i = 5;
//   while( count-- ){
//     i++;
//     cout << count << endl;
//   }
//   cout << i << endl;
// }

void ForWithDynamicArray(){
  // dynamic array
  // need vector library
  std::vector<int> v = { 1,2,3,4,5 };
  for( int i : v ){
    printf("%d\n", i);
  }
}

void ForEach(){
  std::vector<int> v = {1, 2, 3};
  // need algorithm library
  std::for_each(v.begin(), v.end(), [](int i) {
     printf("%d \n", i);
  });

  std::for_each( std::begin(v), std::end(v), [](int i) {
    printf("%d\n", i );
  });

  const char* s = "abcd";
  // need string.h library
  std::for_each( s, s+strlen(s), [](char c) {
    printf("%c\n", c );
  });
}


void Conditions( int i ){
  if ( i < 10 ){
    printf("%s\n", "  OK " );
    printf("%s\n", " wwwOK " );
  }else
    printf("%s\n", "  NotOK " );
}
