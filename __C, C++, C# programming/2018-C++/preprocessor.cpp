#include <iostream>

using namespace std;

/*************************************
 * Function-like Macros
 *************************************/
// #define MIN(a,b)( (a<b)?a:b )

// int main(){

//   int i;
//   double j;
//   i = 100;
//   j = 200.22;

//   cout << MIN(i,j) << endl;

//   return 0;
// }


/*************************************
 * Conditional Comilation
 *************************************/
// #define DEBUG
// #define MIN(a,b)( (a<b)?a:b )

// int main(){

//   int i;
//   double j;
//   i = 100;
//   j = 200.22;

// #ifdef DEBUG
//   cerr << " Inside the main function " << endl;
// #endif

// #if 0
//   cout << MKSTR( HELLO C++ ) << endl;
// #endif
//   cout << MIN(i,j) << endl;

// #ifdef DEBUG
//   cerr << " Coming out the main function " << endl;
// #endif

//   return 0;
// }


/*************************************
 * The # and ## operators
 *************************************/
#define MKSTR( x ) #x
#define concat( i, j ) i ## j

int main(){
  cout << MKSTR( qsdas221$$$ ) << endl;

  int ij = 12200;
  cout << concat( i,j ) << endl;

  return 0;
}


