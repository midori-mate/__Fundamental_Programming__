#include <iostream>

using namespace std;

int main(){
  int var = 20; // actual variable declaration
  int *ip; // pointer variable
  // this should be defined first
  // int* ip2;
  // ip2 = 0;  ------>This is invalid
  // ======================================
  // ip2 = &var;----->This is valid
  int* ip2 = 0;

  ip = &var; // store address of var in pointer variable

  cout << " Value of var variable: ";
  cout << var << endl;

  // print the address stored in ip pointer variable
  cout << " Address stored in ip variable: ";
  cout << ip << endl;

  // access the value at the address avaliable in pointer
  cout << " Value of *ip variable: ";
  cout << *ip << endl;

  // 
  cout << " Value of ip2 variable: ";
  cout << ip2 << endl;

  return 0;
}