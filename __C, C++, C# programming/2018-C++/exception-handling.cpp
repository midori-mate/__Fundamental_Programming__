// #include <iostream>

// using namespace std;

// double division( int x , int y ){
//   if ( y == 0 ){
//     throw "Exception: Division by zero";
//   }
//   return ( x/y );
// }

// int main(){

//   int x = 3;
//   int y = 0;
//   double result = 0;

//   try{
//     result = division(x,y);
//     cout << result << endl;
//   }catch( const char* ex ){
//     cerr << ex << endl;
//   }

//   return 0;
// }

/******************************************
 * Define new exceptions
 ******************************************/
#include <iostream>
#include <exception>

using namespace std;

struct MyZeroException: public exception{
  const char* what() const throw(){
    return " Attention: Zero Division ";
  }
};

int main(){

  try{
    throw MyZeroException();
  }catch( MyZeroException& ex ){
    cerr << ex.what() << endl;
  }catch( exception& ex ){
    //..
  }


  return 0;
}
