#include <iostream>

using namespace std;

class shape{
  // pure virtual function providing interface framework.
  // Abstract classes cannot be used to instantiate objects
  // and serves only as an interface.
  public:
    virtual void area() = 0;
    void setLength( double l ){
      length = l;
    }
    void setWidth( double w ){
      width = w;
    }
  protected:
    double length;
    double width;
};

class Triangle: public shape{
  public:
    void area(){
      cout << length * width * 0.5 << endl;
    }
};

class Rectangle: public shape{
  public:
    void area(){
      cout << length * width << endl;
    }
};


int main(){

  Triangle tri;
  tri.setWidth(20);
  tri.setLength(10);
  tri.area();

  Rectangle rec;
  rec.setWidth(20);
  rec.setLength(10);
  rec.area();


  return 0;
}