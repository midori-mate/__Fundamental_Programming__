#include <iostream>

using namespace std;

class Vol
{
  int id; // private
  public:
    int length;
    int width;
    int height;

    void CalVol(){
      cout << length * width * height << endl;
    };
    friend void printWidth( Vol v );
    void CalArea();
    void GetId(){
      cout << "input a numer: " << endl;
      cin >> id;
      cout << " Your id is " << id << endl;
    };
    static void Add(){
      cout << 1222 << endl;
    };

};
/**
 * It is a member of Vol.
 */
void Vol::CalArea(){
  cout << length*width << endl;
}
/**
 * It is not a member of class Vol.
 * But it can access any member of Vol directly.
 * @param v [description]
 */
void printWidth( Vol v ){
  cout << v.width << endl;
}

int main(){

  Vol v;
  v.length = 10;
  v.width  = 5;
  v.height = 8;

  v.CalVol();
  v.CalArea();
  v.GetId();
  v.Add();

  printWidth( v );

  return 0;
}

