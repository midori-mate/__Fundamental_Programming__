/**
 * references vs Pointers
 * ================================================
 * You cannot have NULL references.
 * You must always be able to assume that a reference is connected to
 * a legitimate piece of storage.
 * -----------------------------------------------------------------------
 * Once a reference is initialized to an object, it cannot be changed
 * to refer to another object. Pointers can be pointed to another object
 * at any time.
 * -----------------------------------------------------------------------
 * A reference must be initialized when it is created.
 * Pointers can be initialized at any time.
 */

#include <iostream>
using namespace std;

void swap( int &x, int &y ){
  int temp;
  temp = x;
  x = y;
  y = temp;

  return;
};


int main(){
  // declare variables
  int i;
  double d;

  // declare reference variables
  int& ri = i;
  double& rd = d;

  i = 5;
  cout << " Value of i is:  " << i << endl;
  cout << " Value of ri is:  " << ri << endl;

  d = 123.3300;
  cout << " Value of d is:  " << d << endl;
  cout << " Value of rd is:  " << rd << endl;

  int k = 10;
  int j = 20;
  swap(k,j);
  cout << " Value of k is:  " << k << endl;
  cout << " Value of j is:  " << j << endl;

  return 0;
}