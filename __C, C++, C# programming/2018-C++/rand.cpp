#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

int random( int start, int end );

int main(){
  int i, j;

  // set the seed
  // srand( (unsigned) time( NULL ) );

  // generate random numbers
  for ( i = 0; i < 10; ++i ){
    // generate actual random number
    // j = rand();
    j = random( 1, 10 );
    cout << " Random Number: " << j << endl;
  }

  return 0;
}


int random( int start, int end ){
  return rand() % end + start;
}