#include <iostream>
#include <ctime>

using namespace std;

// With an inline function, the compiler tries to expand the code
// in the body of the function in place of a call to the function.

inline int add( int x, int y ){
  return (x+y);
}

int _add( int x, int y ){
  return (x+y);
}


int main(){

  int x = 10000;
  int y = 2121212121;

  clock_t s1 = clock();
  cout << " Result1 = " << add( x,y ) << endl;
  clock_t e1 = clock();
  double elapsedtime1 = double(e1-s1)/CLOCKS_PER_SEC;
  cout << " Time is " << elapsedtime1 << endl;

  clock_t s2 = clock();
  cout << " Result2 = " << _add( x,y ) << endl;
  clock_t e2 = clock();
  double elapsedtime2 = double(e2-s2)/CLOCKS_PER_SEC;
  cout << " Time is " << elapsedtime2 << endl;

  return 0;
}