#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

bool nums[10];

/**
 * Return array from function
 */
int * getRandom(){
  static int r[10];
  int i;

  for( i = 0; i < 10; ++i ){
    r[i] = rand();
  }

  return r;
}



int main(int argc, char const *argv[])
{

  // int length = sizeof(nums);

  // for( int i = 0; i < length; ++i ){
  //   printf("%d\n", nums[i] );
  // }

  ////////////////////
  /* a point to an int */
  int *p; //---> store address
  int i;

  p = getRandom();

  for( i = 0; i < 10; ++i ){
    printf("%d\n", *(p+i) );
    printf("%d\n", p[i] );
  }



  return 0;
}