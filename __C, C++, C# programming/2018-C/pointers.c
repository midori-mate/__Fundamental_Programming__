#include <stdio.h>

/**
 * Further information about pointers can
 * refer to the practices of c++ or web page
 */

int main(){

  int var1;
  double var2[10];

  printf("Address of var1 is %x\n", &var1 );
  printf("Address of var2 is %x\n", &var2 );

  return 0;
}