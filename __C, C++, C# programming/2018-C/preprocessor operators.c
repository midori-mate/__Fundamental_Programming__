#include <stdio.h>
#include <stdlib.h>

/**
 * stringize (#) Operator
 */
#define from_mes_to( a, b ) \
  printf( #a "'s message to " #b ": I love you!\n" )

/**
 * Token Pasting (##) operator
 */
#define tokenpaster(n) printf("token" #n " = %d", token##n)

int main(int argc, char const *argv[])
{
  from_mes_to( Alan, Apple );

  int token12 = 120;
  tokenpaster(12);

  exit(EXIT_SUCCESS);
}