#include <stdio.h>

void swap( int *x, int *y ){
  int temp;
  temp = *x;
  *x = *y;
  *y = temp;

  return;
}


int main(int argc, char const *argv[])
{
  int i = 100;
  int j = 200;

  swap(&i,&j);

  printf("i is %d\n", i );
  printf("j is %d\n", j );


  return 0;
}