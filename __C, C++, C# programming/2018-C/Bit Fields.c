#include <stdio.h>
#include <string.h>

/* define simple structure */
struct
{
  unsigned int width;
  unsigned int height;

} status1;

/**
 * The structure requires 8 bytes of memory space.
 * Because the only 0,1 will be saved.
 * The memory space can be utilized by users.
 */

/* define a structure with bit fields */
struct
{
  unsigned int width: 1;
  unsigned int height: 1;

} status2;


int main(int argc, char const *argv[])
{
  printf("Memory size occupied by status1 %d\n", sizeof(status1) );
  printf("Memory size occupied by status2 %d\n", sizeof(status2) );

  return 0;
}
