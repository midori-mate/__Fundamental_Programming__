"use strict";
/**
 * @namespace SHAPP
 */
var SHAPP = SHAPP || {};
SHAPP.name = "My Closure";
SHAPP.Sum = function(a,b){
  return (a+b);
}
SHAPP.Closure = function(){
  let name = "Cl";
  this.getName = function(){
    return name;
  }
}


function myfunc(arg){
  return new Promise((resolve,reject)=>{
    if (arg === 1){
      resolve({A:100,B:200});
    }else{
      reject("not ok");
    }
  });
}

const test = function(a){
  console.log(a);
}

const test2 = function(a){
  console.log(a);
}

myfunc(1).then((received)=>{
  console.log("adadasdasd")
  test(received.A);
  test2(received.B);
});

myfunc(2).then((received)=>{
  console.log("aa"+received);
},(received)=>{
  console.log("ss"+received);
})

var a = "s";
var num = +function(){
  return a;
}();
console.log("ds:  "+num);

var k = (a) => {console.log(a);};
k(1234);


let aa = {aa:11,bb:22}
let keys = Object.keys(aa);
console.log(keys.join(','))