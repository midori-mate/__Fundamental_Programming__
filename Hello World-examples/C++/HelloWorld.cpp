#include <iostream>
#include <string>

using namespace std;

int main(int argc, char const *argv[])
{
  std::string M = "Hello World";
  cout << M << '\n';
  // cout << __cplusplus << '\n';
  M = M.replace(2,3,"2");
  cout << M << endl;
  return 0;
}